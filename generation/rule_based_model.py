from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor
import numpy as np
from scipy import stats
from operator import le, ge
from math import *

from generation.utils import *
from settings import *


def decision_tree(base, samples, min_accuracy=cfg['generation']['decision_tree']['min_accuracy']):
    """
    Train a decision tree on the samples
    :param samples: dataframe: all samples
    :return: sklearn.tree.DecisionTree instance (Classifier or Regression depending of the case)
    """
    samples = samples.dropna()
    X = samples.drop('output', axis=1)
    Y = samples['output']
    if getattr(base, 'categorical_features', False):
        X = base.one_hot_encode(X)
        # # ATTENTION ! POUR UN AFFICHAGE CORRECT DE L'ARBRE DE DÉCISION, IL EST PRÉFÉRABLE
        # # D'INVERSER LES BOOLÉENS. En effet, l'affiche de sklearn présente les variables catégorielles
        # # de la manière suivate : var__is__value <= 0.5 qui renvoie var==value à droite
        # # (double négation). Pour les mettre dans le bon sens, il faut inverser tous les booléens...
        # # ATTENTION AUX UTILISATIONS DE CETTE FONCTION HORS explicoeur
        # for col in X.columns:
        #     if '__is__' in col:
        #         X.loc[:, col] = 1 - X[col]
    max_leaf_nodes = 3
    accuracy = 0
    if base.classification:
        while accuracy < min_accuracy:
            dt = DecisionTreeClassifier(max_leaf_nodes=max_leaf_nodes)
            accuracy = dt.score(X, Y)
            max_leaf_nodes = int(1.334 * max_leaf_nodes)
    else:
        max_leaf_nodes = 15
        dt = DecisionTreeRegressor(max_leaf_nodes=max_leaf_nodes)
        dt.fit(X, Y)
        accuracy = round((np.mean(abs(dt.predict(X) - Y) < 100)) * 100)
        print('n leaves:', max_leaf_nodes, 'R2:', dt.score(X, Y), "accuracy <100 points:", accuracy, "%")
    return dt


def evaluate_model(base, clf):
    current_class = base.model(base.scope)[0]
    precision = clf.rules_[0][1][0]
    complexity = len(clf.rules_[0][0].split('and'))
    scope_of_the_model = clf.predict_top_rules(base.population, n_rules=1)
    real_samples_correctly_pred = base.y_raw[scope_of_the_model.astype(bool)]==current_class
    generality = sum(real_samples_correctly_pred) / float(len(base.population))
    return (complexity, generality, precision)


def parent(node_id, tree_par):
    try:
        return np.where(tree_par.children_right == node_id)[0][0]
    except IndexError:
        return np.where(tree_par.children_left == node_id)[0][0]


def children(node_id, tree_ch):
    if tree_ch.children_right[node_id] == -1:
        return None
    else:
        return [tree_ch.children_right[node_id], tree_ch.children_left[node_id]]


def all_offsprings(node_id, tree_off, current_list=[]):
    direct = children(node_id, tree_off)
    if direct:
        current_list += direct
        left = all_offsprings(direct[0], tree_off, current_list)
        if left:
            current_list += left
        right = all_offsprings(direct[1], tree_off, current_list)
        if right:
            current_list += right
        return list(set(current_list))
    else:
        return None


def all_leaves_id(tree):
    """
    Return the list of all leaves' id
    """
    n_nodes = tree.node_count
    children_left = tree.children_left
    children_right = tree.children_right

    # The tree structure can be traversed to compute various properties such
    # as the depth of each node and whether or not it is a leaf.
    # node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
    leaves = []
    stack = [(0, -1)]  # seed is the root node id and its parent depth
    while len(stack) > 0:
        node_id, parent_depth = stack.pop()
        # node_depth[node_id] = parent_depth + 1

        # If we have a test node
        if (children_left[node_id] != children_right[node_id]):
            stack.append((children_left[node_id], parent_depth + 1))
            stack.append((children_right[node_id], parent_depth + 1))
        else:
            leaves.append(node_id)
    return leaves


def all_leaf_offsprings(node_id, tree):
    offsprings = np.array(all_offsprings(node_id, tree))
    leaves = all_leaves_id(tree)
    return np.array(list(set(offsprings).intersection(set(leaves))))


def sample_closest_leaf_with_mode(sample, mode, tree_cf, X, Y):
    leave_id = tree_cf.apply(sample)
    all_leaves = tree_cf.apply(X)
    current_parent = leave_id
    current_leaves = np.array([])
    current_leaves_mode = np.array([])
    while not any(current_leaves_mode):
        current_parent = parent(current_parent, tree_cf.tree_)
        current_leaves = np.array(all_leaf_offsprings(current_parent, tree_cf.tree_))
        current_leaves_mode = np.array([Y[all_leaves == idx].mode().values[0] == mode for idx in current_leaves])
    counterfact_leaves = current_leaves[current_leaves_mode]  # all leaves with different output
    # select biggest leaf // ON POURRAIT AUSSI SELECTIONNER LA PLUS PURE ?? À TESTER
    leaf_id = np.argmax([sum(all_leaves == leaf) for leaf in counterfact_leaves])
    counterfact_leaf = counterfact_leaves[leaf_id]
    # find a sample that fall into this leaf
    sample_cf = X[all_leaves == counterfact_leaf][-1:]
    return sample_cf


def get_leaf_path_and_info(base, sample, tree_clf, X, Y, leaf_mode, feature_name):
    feature = tree_clf.tree_.feature
    threshold = tree_clf.tree_.threshold

    # First let's retrieve the decision path of each sample. The decision_path
    # method allows to retrieve the node indicator functions. A non zero element of
    # indicator matrix at the position (i, j) indicates that the sample i goes
    # through the node j.
    node_indicator = tree_clf.decision_path(sample)

    # Similarly, we can also have the leaves ids reached by each sample.
    leave_id = tree_clf.apply(sample)

    # Now, it's possible to get the tests that were used to predict a sample or
    # a group of samples. First, let's make it for the sample.
    sample_id = 0
    node_index = node_indicator.indices[node_indicator.indptr[sample_id]:
                                        node_indicator.indptr[sample_id + 1]]

    rules = []
    readable_rules = []
    for node_id in node_index:
        if leave_id[sample_id] == node_id:
            continue
        if sample[0][feature[node_id]] <= threshold[node_id]:
            threshold_sign = "<="
        else:
            threshold_sign = ">"
        readable_rules.append((feature_name[feature[node_id]],
                               rule_symbol_to_operator(threshold_sign),
                               threshold[node_id]))
        rules.append("%s %s %s" % (feature_name[feature[node_id]],
                                   threshold_sign,
                                   threshold[node_id]))
    all_leaves = tree_clf.apply(X)
    Y_node = Y[all_leaves == leave_id]
    Y_not_node = Y[all_leaves != leave_id]
    ttest = stats.ttest_ind(Y_node, Y_not_node, equal_var=False)
    p_val = ttest.pvalue

    n_samples = sum(all_leaves == leave_id)
    if base.classification:
        accuracy = np.mean(Y[all_leaves == leave_id] == leaf_mode)
    elif base.regression:
        accuracy = 1 - (np.std(Y[all_leaves == leave_id]) / np.std(base.y_raw))
    else:
        raise ValueError("base should be regression or classification")
    return {'rules': rules, 'leaf_size': n_samples, 'leaf_purity': accuracy, 'leaf_pvalue': p_val}


def filter_redundant_rules(rules):
    rules_by_feat = {}
    for predicate in rules:
        feat, op, val = predicate.split(' ')
        try:
            rules_by_feat[feat].append((feat, op, val))
        except:
            rules_by_feat[feat] = [(feat, op, val)]

    for k, v in rules_by_feat.items():
        if len(v) > 1:
            all_operators = [pred[1] for pred in v]
            if all_operators.count('>') > 1:
                max_for_gt = np.max([float(pred[2]) for pred in v if pred[1] == '>'])
                keep_idx = list(np.where([
                    pred[1] == '<=' or float(pred[2]) == max_for_gt
                    for pred in v])[0])
                rules_by_feat[k] = [v[idx] for idx in keep_idx]
            if all_operators.count('<=') > 1:
                min_for_le = np.min([float(pred[2]) for pred in v if pred[1] == '<='])
                keep_idx = list(np.where([
                    pred[1] == '>' or float(pred[2]) == min_for_le
                    for pred in v])[0])
                rules_by_feat[k] = [v[idx] for idx in keep_idx]
    pred_list = list(rules_by_feat.values())
    return [' '.join(item) for sublist in pred_list for item in sublist]


def rule_based_model(base, pred_scope, samples, model, min_accuracy=.9, actionable_features=None):
    # fit the rule based model
    samples = samples.dropna()
    X = samples.drop('output', axis=1)
    Y = samples['output']
    if actionable_features is None:
        actionable_features = X.columns
    if getattr(base, 'categorical_features', False):
        X = base.one_hot_encode(X[actionable_features])
        scope = base.one_hot_encode(pred_scope[actionable_features])
        feature_names = scope.columns
    else:
        scope = pred_scope.copy()[actionable_features]
        feature_names = scope.columns

    X = X.values
    scope_output = model(pred_scope)[0]
    scope = scope.values

    max_leaf_nodes_current = 2
    rbms = ([], 0, 0)
    while rbms[2] < min_accuracy and max_leaf_nodes_current < 5000:
        clf = DecisionTreeClassifier(max_leaf_nodes=int(max_leaf_nodes_current))
        y_cond = (Y == scope_output)
        clf = clf.fit(X, y_cond)
        if sum(clf.predict(X)) == 0:
            rbms = ([], 0, 1)
        else:
            leaf_info = get_leaf_path_and_info(base, scope, clf, X, y_cond, 1, feature_names)
            if leaf_info['leaf_pvalue'] < .1:
                rbms = (leaf_info['rules'], leaf_info['leaf_size'], leaf_info['leaf_purity'])
        max_leaf_nodes_current = ceil(max_leaf_nodes_current * 1.1)
    return rbms
