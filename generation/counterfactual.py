import copy
from sampling.selection import *
from generation.utils import *


def example_to_differences(base, scope, counterfactuals):
    cf_list = []
    for row in counterfactuals.iterrows():
        cf = {}
        for col in counterfactuals.columns:
            cf_col_value = row[1][col]
            original_col_value = scope[col].values[0]
            if cf_col_value != original_col_value:
                cf[col] = {'old_val': scope[col].values[0], 'new_val': row[1][col]}
        cf_list.append(cf)
    return cf_list


def example_to_necessary_differences(base, scope, counterfactuals):
    cf_list = []
    original_output = base.model(scope)
    for row in counterfactuals.iterrows():
        cf = {}
        for col in counterfactuals.columns:
            cf_col_value = row[1][col]
            original_col_value = scope[col].values[0]
            if cf_col_value != original_col_value:
                changed = counterfactuals.loc[row[0]:row[0]].replace({col: {cf_col_value: original_col_value}})
                if base.model(changed) != original_output:
                    # the column change is not necessary
                    counterfactuals.loc[row[0]:row[0]] = changed
                else:
                    cf[col] = {'old_val': scope[col].values[0], 'new_val': row[1][col]}
        cf_list.append(cf)
    return cf_list


def find_counterfactual(base, scope, sampling, kwargs_sampling, kwargs_generation, n_cf=10):
    """
    Search for n_cf counterfactuals using the sampling method provided.
    :param base: object: the database object (contains in particular the population and the scope of the explanation)
    :param sampling: function: the sampling method
    :param args_init: dict: arguments to onset the search
    :param n_cf: the target number of counterfactuals
    :param target_class: if specified, the counterfactuals are predicted for this class
    :return: pandas.DataFrame: population of n_cf counterfactuals
    """
    # args = {'sigma': 10, 'rdm_distribution': 'lognormal', 'convert_to_binary_class': False}
    # args = {'n_col_distrib': 'normal', 'normal_sigma': 1, 'convert_to_binary_class': False}
    dynamic_arguments = SamplingArguments(kwargs_sampling)
    current_class = base.model(scope)[0]
    ongoing = True
    count = 0
    while ongoing:
        # sampling
        samples = sampling(base, scope, **dynamic_arguments.arguments())
        n_all_samples = len(samples)
        # only keep cf candidate with a an output different from the scope
        samples = samples[samples['output'] != current_class]
        # heuristic criteria to evaluate the quality of the counterfactual
        if count > 10:
            raise ValueError("No counterfactuals found")
        elif len(samples) == 0:  # no cf found, sampling is too restricted
            dynamic_arguments.broaden()
            args = dynamic_arguments.arguments()
            count += 1
            print("Too narrow, no samples")
        else:  # satisfying number of samples found
            pert_base = copy.copy(base)
            pert_base.population = samples
            samples['sim'] = pert_base.pop_similarity_to_x(scope, features_subset=samples.drop('output', axis=1).columns)
            samples = samples.sort_values('sim')
            samples = samples.loc[~(samples.drop(columns="sim") == scope.values).duplicated(keep='last')]
            selected_cf = samples.tail(n_cf).copy()
            selected_cf.drop_duplicates(inplace=True)
            if kwargs_generation['necessary_diff']:
                return example_to_necessary_differences(base, scope, selected_cf.drop(['output', 'sim'], axis=1))
            else:
                return example_to_differences(base, scope, selected_cf.drop(['output', 'sim'], axis=1))
