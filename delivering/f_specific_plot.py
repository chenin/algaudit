import pylab as plt
import numpy as np
from bokeh.plotting import figure
from bokeh.models import OpenURL, TapTool
from bokeh.layouts import gridplot, layout
from bokeh.embed import components
from bokeh.models import HBar, FactorRange, ColumnDataSource, DataTable, DateFormatter, TableColumn, Div
from bokeh.palettes import RdYlGn
from bokeh.models import LinearAxis, Range1d

from settings import *


def plot_fimportances(imp_dict, title=None):
    """
    Create a pylab plot of features importances
    :param imp_dict: dict: key=column name, value : feature importance
    :return: pylab plot
    """
    imp_dict = {k: v for k, v in sorted(imp_dict.items(), key=lambda item: abs(item[1]))}
    plt.rcParams.update({'font.size': 21})
    fig = plt.figure(figsize=(10, len(imp_dict) * .8))
    y_pos = np.arange(len(imp_dict))
    clrs = ['b' if val>=0 else 'r' for val in imp_dict.values()]
    plt.barh(y_pos, list(imp_dict.values()), align='center', height=0.3, color=clrs)
    if title:
        plt.title(title)
    plt.yticks(y_pos, tuple(imp_dict.keys()))
    plt.savefig("fimportance.pdf", bbox_inches='tight')
    plt.show()


def coef_2d_image(base, imp_dict):
    n_pixels = base.n_pix
    imp_dict = {k: v for k, v in sorted(imp_dict.items(), key=lambda item: int(item[0]))}
    plt.imshow(np.array(list(imp_dict.values())).reshape(n_pixels, n_pixels))
    plt.colorbar()


def plot_partial_dependance(output_pdp):
    fig = plt.figure(figsize=(10, 3.5))
    for k, v in output_pdp.items():
        plt.plot(v[k], v.output)
        plt.xlabel(k)
        plt.ylabel("Mean output")
        plt.title(k)
        plt.show()


def plot_bokeh_pdp(base, pdp_data):
    val_plot = []
    dis_plot = []
    # figure probnp
    y_range_max = max([point['y'] for it in pdp_data.values() for point in it]) * 1.1
    for feature, list_of_points in pdp_data.items():
        # convert data for bokeh
        x = [point['x'] for point in list_of_points]
        y = [point['y'] for point in list_of_points]
        hist = np.array([point['size'] for point in list_of_points])
        hist = hist / sum(hist)

        if feature in base.categorical_features:
            x_sorted_label = [l for l, _ in sorted(zip(x, y), key=lambda it: it[1])]
            if feature in CATE_VALUE_DICT.keys():
                x_sorted_label = [CATE_VALUE_DICT[feature].get(value, value) for value in x_sorted_label]
            fig_val = figure(x_range=x_sorted_label,
                             title=VAR_NAME_DICT[feature],
                             y_range=(0, y_range_max),
                             height=300,
                             height_policy='fixed',
                             x_axis_label=VAR_NAME_DICT[feature],
                             y_axis_label="Score moyen")
            hist_sorted_label = [l for l, _ in sorted(zip(hist, y), key=lambda it: it[1])]
            fig_val.vbar(x=x_sorted_label, top=sorted(y), width=0.9)
            # Setting the second y axis range name and range
            fig_dis = figure(x_range=fig_val.x_range, height=50, height_policy='fixed')
            fig_dis.vbar(x=x_sorted_label, top=hist_sorted_label, width=0.5, color='grey')
        else:
            fig_val = figure(
                 title=VAR_NAME_DICT[feature],
                 y_range=(0, y_range_max),
                 height=300,
                 height_policy='fixed',
                 x_axis_label=VAR_NAME_DICT[feature],
                 y_axis_label="Score moyen")
            fig_val.line(x, y)
            bar_width = ((max(x) - min(x)) / len(x)) * .1
            fig_dis = figure(x_range=fig_val.x_range, height=50, height_policy='fixed')
            fig_dis.vbar(x=x, top=[1] * len(hist), width=bar_width, color='grey')
        fig_val.xaxis.major_tick_line_color = None
        fig_val.xaxis.minor_tick_line_color = None
        fig_val.xaxis.major_label_text_font_size = '0pt'
        fig_dis.yaxis.major_tick_line_color = None
        fig_dis.yaxis.minor_tick_line_color = None
        fig_dis.yaxis.major_label_text_font_size = '0pt'
        fig_dis.xgrid.grid_line_color = None
        fig_dis.ygrid.grid_line_color = None

        val_plot.append(fig_val)
        dis_plot.append(fig_dis)

    colonize_fig = np.array_split(val_plot, len(val_plot) // 3 + 1)
    colonize_dis = np.array_split(dis_plot, len(dis_plot) // 3 + 1)
    all_plots = []
    for i in range(len(colonize_dis)):
        all_plots.append(colonize_fig[i])
        all_plots.append(colonize_dis[i])

    return all_plots