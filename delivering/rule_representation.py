import operator

from sklearn.tree._export import _MPLTreeExporter
from matplotlib.text import Annotation
from sklearn.tree._reingold_tilford import buchheim, Tree
from sklearn import tree
from sklearn.tree import _tree
import networkx as nx
import sklearn.tree as tree
import pylab as plt
import numpy as np
from io import StringIO
from bokeh.models import Plot, Range1d, Rect, HoverTool, TapTool, BoxSelectTool, SaveTool
from bokeh.models.graphs import from_networkx
from bokeh.models.annotations import LabelSet, Label
from bokeh.palettes import Magma256
from scipy.sparse import csr_matrix

def pixel_width(label):
    """
    Width in pixel form rectangle in bokeh decision tree
    :param label:
    :return:
    """
    size = max(map(len, label.split('\n')))
    return size * 3.1


def pixel_height(label):
    """
    Height in pixel form rectangle in bokeh decision tree
    :param label:
    :return:
    """
    return (len([line for line in label.split('\n') if line != '']) + 1) * 10


def set_pos_dict(g, parent, node, pos_dict, dx=1, dy=10, root_coord=(0, 1), eps=0.5):
    if parent is None:
        node = get_root(g)
        x, y = root_coord
    else:
        x, y = pos_dict[parent]
        y = y - dy
        edge = g.get_edge_data(parent, node)
        if edge['name'] == 'yes':
            x = x + dx
        else:
            x = x - dx
    pos_dict[node] = np.array((x, y))

    children = [dest for orig, dest in g.edges if orig == node]
    for child in children:
        set_pos_dict(g, node, child, pos_dict, dx=dx * eps)


def identity(arg):
    return arg


def get_root(g):
    root = [node for node, deg in g.degree() if deg == 2]
    if len(root) != 1:
        raise Exception('something wrong')
    else:
        return root[0]


def tree_to_code(tree, feature_names):
    """
    Outputs a decision tree model as a Python function

    Parameters:
    -----------
    tree: decision tree model
        The decision tree to represent as a function
    feature_names: list
        The feature names of the dataset used for building the decision tree
    """

    tree_ = tree.tree_
    feature_name = [
        feature_names[i] if i != _tree.TREE_UNDEFINED else "undefined!"
        for i in tree_.feature
    ]
    g = nx.DiGraph()

    def recurse(node, depth, g):
        if tree_.feature[node] != _tree.TREE_UNDEFINED:
            name = feature_name[node]
            threshold = tree_.threshold[node]
            categorical = False
            if '__is__' in name:
                name, threshold = name.split('__is__')
                categorical = True
            node_info = {"type": 'parent',
                         "categorical": categorical,
                         'threshold': threshold,
                         'value': round(tree_.value[node][0][0], 1),
                         'name': name,
                         'id': node}
            node_name = repr(node_info)
            g.add_node(node_name)
            cl_name = recurse(tree_.children_left[node], depth + 1, g)
            g.add_edge(node_name, cl_name, name='yes')
            cr_name = recurse(tree_.children_right[node], depth + 1, g)
            g.add_edge(node_name, cr_name, name='no')
        else:
            node_info = {"type": 'leaf',
                         'value': round(tree_.value[node][0][0], 1),
                         'id': node}
            node_name = repr(node_info)
            g.add_node(node_name)
        return node_name

    recurse(0, 1, g)
    return g


def display_decision_tree(dt, feature_names, class_names=None, save_file=False):
    dot_data = StringIO()
    if save_file:
        tree.export_graphviz(dt,
                             out_file=dot_data,
                             feature_names=feature_names,
                             class_names=class_names,
                             filled=True, rounded=True,
                             impurity=False)

        graph = pydot.graph_from_dot_data(dot_data.getvalue())
        graph[0].write_png(save_file)
    img = plt.imread(save_file)
    plt.figure(figsize=(img.shape[0] / 40., img.shape[1] / 40.))
    plt.imshow(img)
    plt.axis('off')
    plt.show()


def display_rules(rule):
    message = ""
    message += "RULE:\n"
    message += "Local model : If:\n\t" + '\n\tand '.join(rule[0].split(' and ')) + "\nthen the output of the model equals the output of the scope with "
    message += "a precision of " + str(rule[1]["precision"])
    print(message)


# def display_rule_2d(rules, n_pixels=cfg['base']['image']['n_pixel']):
#     rule_colors = ['red', 'white', 'blue']
#     legends = ['original value', 'no rule', 'modified']
#     for r in rules:
#         predicate_list = r[0].split(' and ')
#         modified_index = [re.split('>|>=', pred)[0] for pred in predicate_list if ('>' in pred) or ('>=' in pred)]
#         unmodified_index = [re.split('<|<=', pred)[0] for pred in predicate_list if ('<=' in pred) or ('<' in pred)]
#
#         data = np.zeros(n_pixels**2)
#
#         for modif_index in modified_index:
#             if modified_index:
#                 data[int(modif_index)] = 1
#         for unmodif_index in unmodified_index:
#             if unmodified_index:
#                 data[int(unmodif_index)] = -1
#
#         data = data.reshape((n_pixels, n_pixels))
#
#         # create discrete colormap
#         cmap = colors.ListedColormap(rule_colors)
#         bounds = [-1, -.5, +.5, 1]
#         norm = colors.BoundaryNorm(bounds, cmap.N)
#
#         fig, ax = plt.subplots()
#         ax.imshow(data, cmap=cmap, norm=norm)
#
#         patches = [
#             mpatches.Patch(color=rule_colors[i], label=legends[i]) for i in range(len([-1, 0, 1]))
#                    ]
#         # put those patched as legend-handles into the legend
#         plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#
#         plt.show()
