from django.contrib import admin

from cnil.models import *

from import_export.admin import ImportExportActionModelAdmin


class SubjectAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = Subject
    list_display = ['user', 'control_grp', 'created_at', ]
admin.site.register(Subject, SubjectAdmin)


class DecisionExampleAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = DecisionExample
    list_display = ['user', 'case_id', 'created_at', ]
admin.site.register(DecisionExample, DecisionExampleAdmin)


class IbexExampleAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = IbexExample
    # list_display = [field.name for field in model._meta.get_fields()]
admin.site.register(IbexExample, IbexExampleAdmin)


class IbexRequirementAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = IbexRequirement
    list_display = ('user', 'task_ibex', 'simplicity', 'realism', 'format', 'created_at', )
admin.site.register(IbexRequirement, IbexRequirementAdmin)


class LoanFileAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = LoanFile
    # list_display = [field.name for field in model._meta.get_fields()]
admin.site.register(LoanFile, LoanFileAdmin)


class TaskIbexAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = TaskIbex
    list_display = ["user", "case_id", "age_case", "main_factor_answer_user", "main_factor_answer_true", "other_factors_answer_user", "other_factors_answer_true", "extra_question", "extra_question_answer_user", "extra_question_answer_true", "explanation_user", "created_at", "updated_at", ]
admin.site.register(TaskIbex, TaskIbexAdmin)


class IbexFinalQuestionAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = IbexFinalQuestion
    list_display = ["user", "global_factors", "sensible_factors", "expected_behavior", "next_steps", "useful", "pros", "cons", "improvements", "profil", "created_at", ]
admin.site.register(IbexFinalQuestion, IbexFinalQuestionAdmin)


class AlgocateExampleAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = AlgocateExample
admin.site.register(AlgocateExample, AlgocateExampleAdmin)


class ContestAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = Contest
    # list_display = [field.name for field in model._meta.get_fields()]
admin.site.register(Contest, ContestAdmin)


class TaskAlgocateAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = TaskAlgocate
    list_display = ["user", "case_id", "answer_justified_user", "answer_justified_true", "likert_confidence", "reason_user", "created_at", "updated_at", ]
admin.site.register(TaskAlgocate, TaskAlgocateAdmin)


class AlgocateFinalQuestionAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = AlgocateFinalQuestion
    list_display = ["user", "useful", "pros", "cons", "improvements", "diff_ibex", "created_at", ]
admin.site.register(AlgocateFinalQuestion, AlgocateFinalQuestionAdmin)


class CommentFinalAdmin(ImportExportActionModelAdmin):
    list_filter = ('user',)
    model = CommentFinal
    # list_display = [field.name for field in model._meta.get_fields()]
admin.site.register(CommentFinal, CommentFinalAdmin)


