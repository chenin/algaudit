import operator
import copy

from cnil.models import *
from us_cnil.settings import *
from justify.contestation import *


def float_to_str_percent(numeric):
    return str(round(numeric * 100, 2)) + " %"


def is_numeric(obj):
    attrs = ['__add__', '__sub__', '__mul__', '__truediv__', '__pow__']
    return all(hasattr(obj, attr) for attr in attrs)


def new_task_algocate(user, sample_id):
    from cnil.models import TaskAlgocate
    fake_decision = np.random.choice([True, False])
    new_obj = TaskAlgocate(
        user=user,
        case_id=sample_id,
        answer_justified_true=fake_decision,
    )
    new_obj.save()
    return new_obj


def new_task_ibex(user):
    from cnil.models import TaskIbex
    done_case_age = [x.age_case > 60 for x in user.taskibex_set.all()]
    mean_min = np.mean(done_case_age + [False] * (N_IBEX_TASKS - len(done_case_age)))
    mean_max = np.mean(done_case_age + [True] * (N_IBEX_TASKS - len(done_case_age)))
    rdm_draw = np.random.uniform(mean_min, mean_max, 1)[0]
    if rdm_draw < 1. / N_IBEX_TASKS:
        sample_id = EXPLANATIONS[EXPLANATIONS['age'] >= 60]['sample_id'].sample(1).values[0]
    else:
        sample_id = EXPLANATIONS[EXPLANATIONS['age'] < 60]['sample_id'].sample(1).values[0]
    sample = DATA.loc[sample_id:sample_id]
    task_ibex = TaskIbex(
        user=user, case_id=sample_id,
        age_case=sample.age.values[0], main_factor_answer_true='TODO',
        other_factors_answer_true='TODO',
    )
    task_ibex.save()
    return task_ibex


def contestvalue_db_obj_to_internal_class(contest_db_obj, target_feature):
    contest_data = {k: v for k, v in contest_db_obj.__dict__.items()
                    if ('_value' in k or 'operator' in k) and v is not None}
    predicate_list = []
    feature_set = {key.replace('_value', '').replace('_operator', '')
                   for key in contest_data.keys()}
    for feat in feature_set:
        if feat == target_feature:
            target_predicate = (feat, operator.eq, 1)
        else:
            if contest_data.get(feat + '_operator', None) is not None:
                op = convert_operator(contest_data[feat + '_operator'])
                value = contest_data[feat + '_value']
                predicate_list.append((feat, op, value))
    return ContestVal(predicate_list, target_predicate)


def pandas_sample_to_dict(pandas_sample):
    sample_dict = pandas_sample.to_dict('records')[0]
    sample_dict = {
        format_feature_label(k): format_feature_value(k, v)
        for k, v in sample_dict.items()
    }
    return sample_dict


def get_expl_from_req(saved_expl, sample_id, requirements):
    expl = saved_expl[saved_expl['sample_id'] == sample_id].copy()
    expl = expl[expl['explanation'] == requirements['format']]
    if str(requirements['realism']) == '1':
        expl = expl[expl["realism"] == 2]
    elif str(requirements['realism']) == '2':
        expl = expl[expl["realism"] == 3]
    else:
        raise ValueError("Realism should be 1 or 2, not " + str(requirements['realism']))
    if requirements['format'] == "CFactualExplanation":
        expl['explanation_data_str'] = expl['explanation_data'].apply(lambda x: x.keys()).astype(str)
        expl = expl.sort_values(['simplicity', 'generality'], ascending=[True, False]).drop_duplicates(subset='explanation_data_str')
        if str(requirements['simplicity']) == '2':
            return copy.deepcopy(expl.explanation_data.values[0:1])
        else:
            return copy.deepcopy(expl.explanation_data.values[0:2])
    else:
        if str(requirements['simplicity']) == '1':
            if sum(expl["simplicity"] > expl["simplicity"].min()) > 0:
                expl = expl[expl["simplicity"] > expl["simplicity"].min()]
            else:
                expl = expl[expl["simplicity"] == expl["simplicity"].min()]
        elif str(requirements['simplicity']) == '2':
            expl = expl[expl["simplicity"] == expl["simplicity"].min()]
        else:
            raise ValueError("Simplicity should be 1 or 2, not " + str(requirements['simplicity']))
        expl = expl[expl["generality"] == expl["generality"].max()].head(1)
    if requirements['format'] == "RBMExplanation":
        return copy.deepcopy(expl.explanation_data.values[0]), expl.generality.values[0]
    else:
        return copy.deepcopy(expl.explanation_data.values[0])


def convert_feature_name(feature_name):
    if "__is__" in feature_name:
        feat, val = feature_name.split('__is__')
        return format_feature_label(feat) + ' est ' + format_feature_value(feat, val)
    else:
        return format_feature_label(feature_name)


def format_feature_label(label):
    if label in VAR_DICT_NAME.keys():
        return VAR_DICT_NAME[label]
    else:
        return label


def rbm_format_rule(predicate):
    if '__is__' in predicate:
        feat, val = predicate.split('__is__')
        if ' > ' in predicate:
            op = ' est '
        elif ' <= ' in predicate:
            op = " n'est pas "
        else:
            raise ValueError("rule ", predicate, " not well formated")
        val = val.replace(' > 0.5', '').replace(' <= 0.5', '')
        formated_pred = format_feature_label(feat) + op + format_feature_value(feat, val)
    elif "pan_flag" in predicate or "voterid_flag" in predicate:
        op = ' est '
        if ' > ' in predicate:
            feat, _ = predicate.split(' > ')
            val = 1
        elif ' <= ' in predicate:
            feat, _ = predicate.split(' <= ')
            val = 0
        else:
            raise ValueError("rule ", predicate, " not well formated")
        formated_pred = format_feature_label(feat) + op + format_feature_value(feat, val)
    else:
        for op in [' <= ', ' > ', ' == ', ' est ', " n'est pas "]:
            if len(predicate.split(op)) > 1:
                feat, val = predicate.split(op)
                formated_pred = format_feature_label(feat) + op + str(format_feature_value(feat, val))
    return formated_pred


def format_feature_value(feat, value):
    try:
        value = int(str(value))
    except ValueError:
        try:
            value = float(str(value))
        except ValueError:
            None
    if feat == 'contrib_percentage':
        return str(round(value, 2)) + ' %'
    if feat in ['average_acct_age', 'credit_history_length']:
        year = int(value)
        month = int((value - int(value)) * 12)
        if year <= 1:
            return str(month) + ' mois'
        if year == 1:
            return str(year) + ' an ' + str(month) +' mois'
        if year >= 1:
            return str(year) + ' ans ' + str(month) + ' mois'
    if feat == 'age':
        year = int(value)
        return str(year) + ' ans '
    if feat in ["pan_flag", "voterid_flag"]:
        if feat in CATE_VALUE_DICT.keys():
            return CATE_VALUE_DICT[feat][value]
        else:
            return value
    elif is_numeric(value):
        if type(value)==int:
            return str(value)
        else:
            return str(round(value, 2))
    else:
        if feat in CATE_VALUE_DICT.keys():
            return CATE_VALUE_DICT[feat][value]
        else:
            return value


def convert_operator(symbol):
    if symbol == 'est':
        return operator.eq
    elif symbol == '=':
        return operator.eq
    elif symbol == "n'est pas":
        return operator.ne
    elif symbol == '>=':
        return operator.ge
    elif symbol == '<=':
        return operator.le
    else:
        raise ValueError("Symbol " + symbol + " is not valid. ")

