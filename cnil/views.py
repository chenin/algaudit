
# Create your views here.
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.models import HBar, FactorRange, ColumnDataSource
from bokeh.palettes import RdYlGn
from bokeh.models.widgets import DataTable, TableColumn, HTMLTemplateFormatter
import string
import random

from django.views.generic import TemplateView
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from cnil.forms import *
from cnil.utils import *
from justify.rule_based_justifications import *
from justify.norm import *
from justify.deliver import *
from delivering.rule_representation import *
from generation.rule_based_model import *


# # # # #  VIEWS INTRODUCTION  # # # # #
class WelcomeView(TemplateView):
    template_name = "home.html"

    def post(self, request, *args, **kwargs):
        if "restart" in request.POST:
            logout(request)
            return render(request, 'home.html')
        else:
            return render(request, 'home.html')


class PresentationView(TemplateView):
    template_name = "presentation.html"


class ConsentView(TemplateView):
    template_name = "consent.html"

    # def get(self, request, *args, **kwargs):
    #     if self.request.user.is_authenticated:
    #         return redirect('features_presentation')
    #     else:
    #         return render(request, 'consent.html')

    def post(self, request, *args, **kwargs):
        if "start" in request.POST:
            if not request.user.is_authenticated:
                uname = 'HC_' + ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
                pword = 'mdp_par_defaut'
                user = User.objects.create_user(uname, 'no@mail.com', pword)
                user.save()
                user = authenticate(request, username=uname, password=pword)
                subject = Subject(user=user, control_grp=False)
                subject.save()
                if user is not None:
                    login(request, user)
                return redirect('features_presentation')
        else:
            return render(request, 'consent.html')


class FeaturesPresentationView(TemplateView):
    template_name = "features_presentation.html"


class DecisionExampleView(TemplateView):
    template_name = "example.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Select sample to be displayed as example (or recover existing one)
        if self.request.session.get('example_sample_id', False):
            sample_id = self.request.session['example_sample_id']
            sample = DATA.loc[sample_id:sample_id]
        else:
            example_ids = [74469, 6701, 13841, 13734, 16203]
            done_ids = [x.case_id for x in self.request.user.decisionexample_set.all()]
            sample_id = np.random.choice([x for x in example_ids if x not in done_ids])
            self.request.session['example_sample_id'] = int(sample_id)
            sample = DATA.loc[sample_id:sample_id]
            ex = DecisionExample(user=self.request.user, case_id=sample.index[0])
            ex.save()
        context['sample_output'] = sample.decision.values[0]
        context['sample'] = pandas_sample_to_dict(sample)
        return context

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            if self.request.user.decisionexample_set.count() < N_DECISION_EXAMPLES:
                context = self.get_context_data(**kwargs)
                return render(request, 'decision_example.html', context)
            else:
                return redirect('decision_example_final')
        else:
            msg_txt = "Le serveur ne vous a pas reconnu et vous a redirigé vers la page d'accueil"
            messages.add_message(request, messages.INFO, msg_txt)
            return redirect('welcome_page')

    def post(self, request, *args, **kwargs):
        if "next_example" in request.POST:
            del self.request.session['example_sample_id']
            return redirect("decision_example")


class DecisionExampleFinalView(TemplateView):
    template_name = "decision_example_final.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        samples_ids = [x.case_id for x in self.request.user.decisionexample_set.all()]
        samples = DATA.loc[samples_ids]
        context['samples'] = pandas_sample_to_dict(samples)

        # Classements
        for col in samples.columns:
            samples.loc[:, col] = samples[col].apply(lambda x: format_feature_value(col, x))
        samples = samples.drop('y_true', axis=1)
        samples = samples.replace(CATE_VALUE_DICT).transpose()
        samples.columns = ['1', '2', '3', '4', '5']
        samples.reset_index(inplace=True)
        samples['index'].replace(VAR_DICT_NAME, inplace=True)
        table_data = samples.to_dict('series')
        table_data = {k: v.values for k, v in table_data.items()}

        source = ColumnDataSource(table_data)
        columns = [TableColumn(field='index', title="Variable", width=None)]
        columns += [
            TableColumn(field=it, title="Exemple " + it, width=None) for it in ['1', '2', '3', '4', '5']
        ]
        rankings_table = DataTable(source=source, columns=columns,
                                   autosize_mode="fit_columns", index_position=None,
                                   sizing_mode="scale_width", )

        script, div = components(rankings_table, )
        context['script'] =script
        context['div'] = div
        return context


class ChoiceToolView(TemplateView):
    template_name = "choose_tool.html"



# # # # #  VIEWS IBEX  # # # # #
class PresentationIbexView(TemplateView):
    template_name = "ibex_presentation.html"


class IbexView(TemplateView):

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        # Select sample to be displayed as example (or recover existing one)
        if "example" in self.template_name:
            if self.request.session.get('ibex_example_id', False):
                example = IbexExample.objects.get(pk=self.request.session['ibex_example_id'])
                sample_id = example.case_id
                sample = DATA.loc[sample_id:sample_id]
            else:
                sample_id = EXPLANATIONS['sample_id'].sample(1).values[0]
                sample = DATA.loc[sample_id:sample_id]
                example = IbexExample(user=self.request.user, case_id=sample_id)
                example.save()
                self.request.session['ibex_example_id'] = int(example.pk)
        else:
            if self.request.session.get('ibex_task_id', False):
                task = TaskIbex.objects.get(pk=self.request.session['ibex_task_id'])
                task.save()
                sample_id = task.case_id
                sample = DATA.loc[sample_id:sample_id]
            else:
                task_ibex = new_task_ibex(self.request.user)
                task_ibex.save()
                sample_id = task_ibex.case_id
                sample = DATA.loc[sample_id:sample_id]
                self.request.session['ibex_task_id'] = int(task_ibex.pk)

        # Get explanation requirement (or default)
        if kwargs.get("requirements", False):
            requirements = kwargs.get("requirements")
        else:
            requirements = DEFAULT_REQUIREMENTS

        if kwargs.get("calculette_input", False):
            dict_data = {k: [v] for k, v in kwargs["calculette_input"].items()}
            decision = BASE.model(pd.DataFrame.from_dict(dict_data))[0]
            context['calculette_decision'] = decision

        requirement_form = IbexRequirementForm(initial=requirements)
        context["current_requirements"] = requirements
        context['requirement_form'] = requirement_form
        sample_output = sample['decision'].values[0]
        context['sample_output'] = sample_output
        context['sample'] = pandas_sample_to_dict(sample)
        context['ibex_task_form'] = TaskIbexForm()
        init_values = sample.to_dict("records")[0]
        # init_values['employment_type'] = CATE_VALUE_DICT['employment_type'][init_values['employment_type']]
        for var in ['average_acct_age', 'credit_history_length', 'age']:
            init_values[var] = round(init_values[var], 2)
        calculette_form = LoanFileForm(initial=init_values)
        context['calculette_form'] = calculette_form

        # # # # # # EXPLANATIONS # # # # # #
        if self.request.user.is_authenticated:
            orange = RdYlGn[3][2]
            green = RdYlGn[3][0]
            explanation_data = get_expl_from_req(EXPLANATIONS, sample.index[0], requirements)
            context['explanation_data'] = explanation_data
            context['explanation_type'] = requirements['format']
            context['realism'] = str(requirements['realism'])
            if requirements['format'] == "FImportanceExplanation":
                fi = explanation_data
                features_name_fi_plot = [convert_feature_name(label) for label in fi.keys()]
                values_fi_plot = list(fi.values())
                values_fi_plot, features_name_fi_plot = zip(
                    *sorted(zip(values_fi_plot, features_name_fi_plot)))  # sort by feature importance value
                colors = [orange if val < 0 else green for val in values_fi_plot]
                TOOLS = "save"
                # instantiating the figure object
                fi_plot = figure(title="Importance relative des variables",
                                 y_range=FactorRange(factors=features_name_fi_plot),
                                 plot_height=max(len(values_fi_plot) * 30, 200),
                                 max_width=800,
                                 tools=TOOLS,
                                 align='center',
                                 toolbar_location=None)
                source = ColumnDataSource(dict(y=features_name_fi_plot, right=values_fi_plot, color=colors))
                glyph = HBar(y="y", right="right", fill_color='color', left=0, height=0.3)
                fi_plot.add_glyph(source, glyph)
                fi_plot.xaxis.major_tick_line_color = None  # turn off x-axis major ticks
                fi_plot.xaxis.minor_tick_line_color = None  # turn off x-axis minor ticks
                fi_plot.xaxis.major_label_text_font_size = '0pt'  # preferred method for removing tick labels

                script, div = components(fi_plot)
                context['script'] = script
                context['div'] = div
            if requirements['format'] == "RBMExplanation":
                rules, generality = explanation_data
                context['rules'] = [rbm_format_rule(r) for r in filter_redundant_rules(rules)]
                context['rule_decision'] = CLASS_NAMES[sample_output]
                context['generality'] = str(round(generality * 100, 2)) + ' %'
            if requirements['format'] == "CFactualExplanation":
                if requirements["realism"] in [1, '1']:
                    # display counterfactuals as modifications to original pred
                    counterfact_list = []
                    for cf in explanation_data:
                        counterfact = {format_feature_label(k): format_feature_value(k, val['new_val']) for k, val in cf.items()}
                        counterfact_list.append(counterfact)
                    context['counterfact_list'] = counterfact_list
                    context['sample_decision_text'] = CLASS_NAMES[sample_output]
                    context['cf_decision_text'] = CLASS_NAMES[1 - sample_output]
                elif requirements["realism"] in [2, '2']:
                    # display counterfactuals as alternatives examples
                    realistic_cf = sample.drop('y_true', axis=1).copy()
                    for replace_dict in explanation_data:
                        modified_sample = sample.drop('y_true', axis=1).copy()
                        for k, v in replace_dict.items():
                            replace_dict[k] = {v['old_val']: v['new_val']}
                        modified_sample.loc[:, 'decision'] = 1 - modified_sample['decision']
                        modified_sample = modified_sample.replace(replace_dict)
                        realistic_cf = pd.concat([realistic_cf, modified_sample])

                    for col in realistic_cf.columns:
                        realistic_cf.loc[:, col] = realistic_cf[col].apply(lambda x: format_feature_value(col, x))
                    realistic_cf = realistic_cf.replace(CATE_VALUE_DICT).transpose()
                    realistic_cf.reset_index(inplace=True)
                    realistic_cf.replace(VAR_DICT_NAME, inplace=True)

                    columns_label = [("var", "Variable"), ("file_val", "Dossier d'origine"), ]
                    for it in range(len(explanation_data)):
                        columns_label.append(("cf_" + str(it+1), "Contrefactuelle " + str(it + 1)))
                    realistic_cf.columns = [c[0] for c in columns_label]

                    table_data = realistic_cf.to_dict('series')
                    table_data = {k: v.values for k, v in table_data.items()}

                    source = ColumnDataSource(table_data)
                    columns = [
                        TableColumn(field=col[0], title=col[1], width=None) for col in columns_label
                    ]
                    rankings_table = DataTable(source=source, columns=columns,
                                               autosize_mode="fit_columns", index_position=None,
                                               sizing_mode="scale_width", )

                    script, div = components(rankings_table, )
                    context['script'] = script
                    context['div'] = div
                    context["explanation_data"] = explanation_data
                else:
                    raise ValueError("Realism should be 1 or 2")
        return context

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            if "example" in self.template_name:
                if self.request.user.ibexexample_set.count() < N_IBEX_EXAMPLES:
                    context = self.get_context_data(**kwargs)
                    return render(request, 'ibex_example.html', context)
                else:
                    return redirect('ibex_task')
            else:
                if self.request.user.taskibex_set.exclude(main_factor_answer_user__isnull=True).count() < N_IBEX_TASKS:
                    context = self.get_context_data(**kwargs)
                    return render(request, 'ibex_task.html', context)
                else:
                    return redirect('ibex_final')
        else:
            msg_txt = "Le serveur ne vous a pas reconnu et vous a redirigé vers la page d'accueil"
            messages.add_message(request, messages.INFO, msg_txt)
            return redirect('welcome_page')

    def post(self, request, *args, **kwargs):

        if "next_example" in request.POST:
            del self.request.session['ibex_example_id']
            return redirect("ibex_example")

        if "ibex_task_form" in request.POST:
            id = self.request.session['ibex_task_id']
            task_ibex = TaskIbex.objects.get(pk=id)
            form = TaskIbexForm(request.POST)
            if form.is_valid():
                task_ibex.main_factor_answer_user = form.cleaned_data['main_factor_answer_user']
                task_ibex.other_factors_answer_user = form.cleaned_data['other_factors_answer_user']
                # task_ibex.extra_question_answer_user = form.cleaned_data['extra_question_answer_user']
                task_ibex.explanation_user = form.cleaned_data['explanation_user']
                task_ibex.save()
                del self.request.session['ibex_task_id']
                return redirect("ibex_task")
            else:
                context = self.get_context_data(**kwargs)
                context['ibex_task_form'] = form
                return render(request, self.template_name, context)

        if "requirement_form" in request.POST:
            form = IbexRequirementForm(request.POST)
            if form.is_valid():
                requirement = form.save(commit=False)
                requirement.user = self.request.user
                if "example" not in self.template_name:
                    ibex_task_id = self.request.session['ibex_task_id']
                    task = TaskIbex.objects.get(pk=ibex_task_id)
                    requirement.task_ibex = task
                requirement.save()
                requirements = {
                    "simplicity": form.data['simplicity'],
                    "realism": form.data['realism'],
                    "format": form.data['format'],
                }
                context = self.get_context_data(requirements=requirements, **kwargs)
                return render(request, self.template_name, context)

        if "calculette_form" in request.POST:
            form = LoanFileForm(request.POST)
            if form.is_valid():
                loan_file = form.save(commit=False)
                calculette_input = loan_file.__dict__
                loan_file.user = self.request.user
                if "example" not in self.template_name:
                    ibex_task_id = self.request.session['ibex_task_id']
                    task = TaskIbex.objects.get(pk=ibex_task_id)
                    loan_file.task_ibex = task
                loan_file.save()
                context = self.get_context_data(calculette_input=calculette_input, **kwargs)
                context['calc_tab'] = True
                context['calculette_form'] = form
                return render(request, self.template_name, context)
            else:
                context = self.get_context_data(**kwargs)
                context['calculette_form'] = form
                context['calc_tab'] = True
                return render(request, self.template_name, context)



class FinalIbexView(TemplateView):
    template_name = "ibex_final.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ibex_final_form'] = IbexFinalQuestionForm()
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if "ibex_final_form" in request.POST:
            form = IbexFinalQuestionForm(request.POST)
            context['ibex_final_form'] = form
            if form.is_valid():
                ibex_final_questions = form.save(commit=False)
                ibex_final_questions.user = self.request.user
                ibex_final_questions.save()
                return redirect("thanks")
        return render(request, 'ibex_final.html', context)


# # # # #  VIEWS ALGOCATE  # # # # #
class PresAlgocateView(TemplateView):
    template_name = "algocate_presentation.html"


class AlgocateView(TemplateView):

    def __init__(self, *args, **kwargs):
        super(AlgocateView, self).__init__(*args, **kwargs)
        self.template_name = kwargs['template_name']

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        # Select sample to be displayed as example (or recover existing one)
        if "example" in self.template_name:
            if self.request.session.get('algocate_example_id', False):
                example = AlgocateExample.objects.get(pk=self.request.session['algocate_example_id'])
                sample_id = example.case_id
                sample = DATA.loc[sample_id:sample_id]
            else:
                sample = DATA.sample(1)
                sample_id = sample.index[0]
                example = AlgocateExample(user=self.request.user, case_id=sample_id)
                example.save()
                self.request.session['algocate_example_id'] = int(example.pk)
        else:
            if self.request.session.get('algocate_task_id', False):
                task = TaskAlgocate.objects.get(pk=self.request.session['algocate_task_id'])
                sample_id = task.case_id
                sample = DATA.loc[sample_id:sample_id].copy()
                if task.answer_justified_true:
                    # flip the decision to artificially create an unjustified decision
                    sample.loc[:, 'decision'] = 1 - sample.loc[:, 'decision']
            else:
                sample = DATA.sample(1).copy()
                sample_id = sample.index[0]
                task_algocate = new_task_algocate(self.request.user, sample_id)
                if task_algocate.answer_justified_true:
                    # flip the decision to artificially create an unjustified decision
                    sample.loc[:, 'decision'] = 1 - sample.loc[:, 'decision']
                self.request.session['algocate_task_id'] = int(task_algocate.pk)

        context['sample_output'] = sample['decision'].values[0]
        context['pd_sample'] = sample
        context['sample'] = pandas_sample_to_dict(sample)
        file = {k: format_feature_value(k, v) for k, v in sample.to_dict('records')[0].items()}
        context['file'] = file
        algocate_contest_form = ContestValueForm()
        context['algocate_contest_form'] = algocate_contest_form
        task_form = TaskAlgocateForm()
        context['algocate_task_form'] = task_form
        return context

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            if "example" in self.template_name:
                if self.request.user.algocateexample_set.count() < N_ALGOCATE_EXAMPLES:
                    context = self.get_context_data(**kwargs)
                    return render(request, 'algocate_example.html', context)
                else:
                    return redirect('algocate_task')
            else:
                if self.request.user.taskalgocate_set.exclude(answer_justified_user__isnull=True).count() < N_ALGOCATE_TASKS:
                    context = self.get_context_data(**kwargs)
                    return render(request, 'algocate_task.html', context)
                else:
                    return redirect('algocate_final')
        else:
            msg_txt = "Le serveur ne vous a pas reconnu et vous a redirigé vers la page d'accueil"
            messages.add_message(request, messages.INFO, msg_txt)
            return redirect('welcome_page')

    def post(self, request, *args, **kwargs):
        if "next_example" in request.POST:
            del self.request.session['algocate_example_id']
            return redirect("algocate_example")

        context = self.get_context_data(**kwargs)
        if "algocate_task_form" in request.POST:
            form = TaskAlgocateForm(request.POST)
            if form.is_valid():
                task_algocate_obj = TaskAlgocate.objects.get(pk=self.request.session['algocate_task_id'])
                task_algocate_obj.answer_justified_user = form.cleaned_data['answer_justified_user']
                task_algocate_obj.likert_confidence = form.cleaned_data['likert_confidence']
                task_algocate_obj.reason_user = form.cleaned_data['reason_user']
                task_algocate_obj.save()
                del self.request.session['algocate_task_id']
                return redirect("algocate_task")
            else:
                context = self.get_context_data(**kwargs)
                context['algocate_task_form'] = form
                return render(request, self.template_name, context)

        if "contest_form" in request.POST:
            form = ContestValueForm(request.POST, file=context['pd_sample'].to_dict('records')[0])
            if form.is_valid():
                contest_value = form.save(commit=False)
                contest_value.target_output = 1 - context['sample_output']
                contest_value.user = self.request.user
                if "example" not in self.template_name:
                    algocate_task_id = self.request.session['algocate_task_id']
                    task = TaskAlgocate.objects.get(pk=algocate_task_id)
                    contest_value.task_algocate = task
                contest_value.save()
                cv = contestvalue_db_obj_to_internal_class(contest_value, 'y_true')
                context["predicates"] = ' et '.join(
                            [format_feature_label(r[0]) + ' ' + operator_to_symbol(r[1]) + ' ' + format_feature_value(r[0], r[2]) for r in cv.predicate_list]
                        )
                context["target_output"] = CLASS_NAMES[contest_value.y_true_value]
                sample = context['pd_sample']
                all_norms = [CustomRule2(), CustomRule1(), ReferenceNorm(), ]
                contest_dataset = justification_dataset(cv, DATA, 'undetermined')
                all_norms_info = []
                for norm in all_norms:
                    if norm.type in ['objective', 'reference']:
                        evidence = norm.evidence(cv, DATA, contest_dataset)
                        norm_data = {
                            'type': norm.type, 'name': str(norm),
                            'value': evidence['value'], 'avg': evidence['average'],
                            'size': evidence['size'], 'pvalue': evidence['pvalue'],
                            'ttest': evidence['ttest'], 'description': norm.description,
                        }
                        rbm, rbm_neg = pro_and_cons_rules(sample, cv, norm, DATA)
                        if rbm:
                            norm_data['rbm_predicate'] = ' et '.join(
                                [format_feature_label(r[0]) + ' ' + r[1] + ' ' + format_feature_value(r[0], r[2]) for r in rbm['rbm_predicates']]
                            )
                            norm_data['rbm_ttest'] = rbm['ttest']
                            norm_data['rbm_value'] = rbm['value']
                            norm_data['rbm_size'] = rbm['size']
                        else:
                            norm_data['rbm_ttest'] = 0
                        if rbm_neg:
                            norm_data['rbm_neg_predicate'] = ' et '.join(
                                [format_feature_label(r[0]) + ' ' + r[1] + ' ' + format_feature_value(r[0], r[2]) for r in rbm_neg['rbm_predicates']]
                            )
                            norm_data['rbm_neg_ttest'] = rbm_neg['ttest']
                            norm_data['rbm_neg_value'] = rbm_neg['value']
                            norm_data['rbm_neg_size'] = rbm_neg['size']
                        else:
                            norm_data['rbm_neg_ttest'] = 0
                    elif norm.type == 'rule':
                        norm_data = {
                            'type': norm.type, 'name': str(norm),
                            'description': norm.description, 'test': norm.test(sample),
                            'output_value': norm.output_value,
                        }
                    else:
                        raise ValueError("Norm type should be objective, reference or rule, not " + str(norm.type))
                    all_norms_info.append(norm_data)
                context["justification_text"] = justification_formatting(
                    all_norms_info, context["target_output"], context['predicates']
                )
            else:
                context = self.get_context_data(**kwargs)
                context['algocate_contest_form'] = form
                return render(request, self.template_name, context)

        return render(request, self.template_name, context)


class TaskAlgocateView(TemplateView):
    template_name = "algocate_presentation.html"


class FinalAlgocateView(TemplateView):
    template_name = "algocate_final.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['algocate_final_form'] = AlgocateFinalQuestionForm()
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if "algocate_final_form" in request.POST:
            form = AlgocateFinalQuestionForm(request.POST)
            context['algocate_final_form'] = form
            if form.is_valid():
                algocate_final_questions = form.save(commit=False)
                algocate_final_questions.user = self.request.user
                algocate_final_questions.save()
                return redirect("thanks")
        return render(request, 'algocate_final.html', context)


class Thanks(TemplateView):
    form_class = FinalForm
    template_name = 'thanks.html'

    def get_context_data(self, request, *args, **kwargs):
        context = {}
        context['total_comment'] = self.request.user.commentfinal_set.count()
        return context

    def get(self, request, *args, **kwargs):
        total_comment = self.request.user.commentfinal_set.count()
        form = self.form_class()
        return render(request, self.template_name, {'form': form, 'total_comment': total_comment})

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(self, request, *args, **kwargs)
        form = self.form_class(request.POST)
        if form.is_valid():
            user = self.request.user
            if form.cleaned_data['mail']:
                user.email = form.cleaned_data['mail']
                user.save()
            if form.cleaned_data['comment']:
                f_comm = CommentFinal(user=user, comment=form.cleaned_data['comment'])
                f_comm.save()
            context['form'] = self.form_class(request.POST)
            return render(request, 'thanks.html', context)

        context['form'] = self.form_class()
        return render(request, self.template_name, context)














