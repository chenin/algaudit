from django.forms import ModelForm
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit
from crispy_forms.bootstrap import InlineRadios
from crispy_forms.layout import Div
from django.forms import ModelForm, Textarea

from cnil.models import *
from cnil.utils import *

CASE_CHOICE = (
    (True, "Accepter"),
    (False, "Contester"),
)
LIKERT_CHOICES = (
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
)


# IBEX FORMS

class IbexRequirementForm(ModelForm):
    class Meta:
        model = IbexRequirement
        fields = ['simplicity', 'realism', 'format']

    def __init__(self, *args, **kwargs):
        super(IbexRequirementForm, self).__init__(*args, **kwargs)
        self.fields['simplicity'].label = "Simplicité"
        self.fields['realism'].label = "Réalisme"
        self.fields['format'].label = "Forme"


class TaskIbexForm(ModelForm):
    class Meta:
        model = TaskIbex
        fields = ['main_factor_answer_user', 'explanation_user']

    other_factors_answer_user = forms.MultipleChoiceField(
        required=True,
        widget=forms.CheckboxSelectMultiple,
        choices=CHOICE_FEATURE,
    )
    field_order = ['main_factor_answer_user', 'other_factors_answer_user', 'explanation_user']

    def __init__(self, *args, **kwargs):
        super(TaskIbexForm, self).__init__(*args, **kwargs)
        self.fields['main_factor_answer_user'].label = "Quel a été le facteur déterminant de la décision ?"
        self.fields['other_factors_answer_user'].label = "D’autres facteurs ont-ils influencé la décision ?"
        self.fields['explanation_user'].label = "Comment comprenez vous la décision ? S'agit-il d'un cas limite ou d'un simple ?"
        self.fields['main_factor_answer_user'].required = True
        self.fields['other_factors_answer_user'].required = False
        self.fields['explanation_user'].required = True


class LoanFileForm(ModelForm):
    class Meta:
        model = LoanFile
        fields = [
            # 'disbursed_amount',
            'asset_cost', 'contrib_percentage',
            'employment_type', 'pri_no_of_accts', 'pri_active_accts',
            'pri_overdue_accts', 'pri_current_balance',
            'primary_instal_amt', 'new_accts_in_last_six_months',
            'delinquent_accts_in_last_six_months', 'average_acct_age',
            'credit_history_length', 'no_of_inquiries', 'age',
        ]

    def __init__(self, *args, **kwargs):
        super(LoanFileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-6'
        self.helper.field_class = 'col-lg-4'
        self.helper.add_input(Submit('calculette_form', 'Simuler la décision', css_class='btn-primary'))
        self.helper.form_method = 'POST'



class IbexFinalQuestionForm(ModelForm):
    class Meta:
        model = IbexFinalQuestion
        fields = ['global_factors', 'sensible_factors',
                  # 'expected_behavior', 'next_steps',
                  'useful',
                  'pros', 'cons', 'improvements', 'profil',]

    def __init__(self, *args, **kwargs):
        super(IbexFinalQuestionForm, self).__init__(*args, **kwargs)
        self.fields['global_factors'].label = "D’après les analyses que vous avez réalisées, quelles variables sont globalement les plus importantes ?"
        # self.fields['expected_behavior'].label = "Le fonctionnement de l’algorithme correspond-il à ce que décrit la société ? Certaines décisions vous ont-elles semblées discriminantes envers une partie de la population ?"
        self.fields['sensible_factors'].label = "Estimez-vous que certaines données sont collectées de manière injustifiée (informations qui ne seraient pas utilisées) ? Toutes les données traitées sont-elles pertinentes et/ou nécessaires ?"
        # self.fields['next_steps'].label = "Quelles sont les suites à donner ? Faut-il proposer de : i) clore le dossier, ii) poursuivre les investigation ou iii) entamer une procédure de mise en demeure ou de sanction ? "
        self.fields['useful'].label = "L’outil IBEX vous semble-t-il adapté pour faciliter la compréhension d’un système algorithmique ?"
        self.fields['pros'].label = "Quels sont selon vous ses avantages (s’il y en a) ?"
        self.fields['cons'].label = "Quels sont selon vous ses inconvénients (s’il y en a) ?"
        self.fields['improvements'].label = "Voyez-vous des pistes d’amélioration ?"
        self.fields['profil'].label = "Quel est votre profil ?"


class FinalForm(forms.Form):
    mail = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Entrez votre email (optionel)'}))
    comment = forms.CharField(label="Commentaire", required=False, widget=forms.Textarea(attrs={
        'style': 'height: 20em; width: 50em;', 'placeholder': 'Commentaire libre (optionel)'}))


# ALGOCATE FORMS

class ContestValueForm(ModelForm):
    class Meta:
        model = Contest
        exclude = ['task_algocate', 'user']

    def __init__(self, *args, **kwargs):
        file = kwargs.pop('file', None)
        super(ContestValueForm, self).__init__(*args, **kwargs)
        self.file = file
        self.fields['average_acct_age_value'].help_text = "Durée en année"
        self.fields['credit_history_length_value'].help_text = "Durée en année"

    helper = FormHelper()
    # helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-12'
    helper.field_class = 'col-lg-12'
    helper.form_show_labels = False

    def clean(self, *args, **kwargs):
        """
        In here you can validate the two fields
        raise ValidationError if you see anything goes wrong.
        for example if you want to make sure that field1 != field2
        """
        data = super().__dict__
        cleaned_data = super().clean()
        if sum([x is not None for x in cleaned_data.values()]) == 1:
            self._errors['asset_cost_value'] = ["Au moins une condition doit être ajoutée. Vous pouvez utiliser la condition Prix du vehicule >= 0 si vous ne savez pas quoi mettre. "]
        f_names = [f.name for f in Contest._meta.get_fields()]
        model_fields = {name.replace('_operator', '').replace('_value', '')
                        for name in f_names if '_operator' in name or '_value' in name}
        for field in list(model_fields):
            field_val = cleaned_data[field + '_value']
            if field == "y_true":
                if field_val is None:
                    self._errors[field + '_value'] = [
                        "Ce champs doit être rempli"]  # Will raise a error message
            else:
                field_op = cleaned_data[field + '_operator']
                if (field_op is None) != (field_val is None):
                    self._errors[field + '_value'] = ["L'opérateur ET la valeur doivent être remplis ou laissés vides tous les deux"]  # Will raise a error message
                elif ((field_op is not None) and
                    not convert_operator(field_op)(self.file[field], field_val)):
                    self._errors[field + '_value'] = ["Le dossier doit satisfaire cette contrainte"]  # Will raise a error message
        return cleaned_data


class TaskAlgocateForm(ModelForm):
    class Meta:
        model = TaskAlgocate
        fields = ['answer_justified_user', 'likert_confidence', 'reason_user']

    def __init__(self, *args, **kwargs):
        super(TaskAlgocateForm, self).__init__(*args, **kwargs)
        self.fields['answer_justified_user'].label = "La décision vous paraît-elle justifiée au regard des normes déclarées par la société ?"
        self.fields['likert_confidence'].label = "Indiquer le niveau de confiance de votre réponse"
        self.fields['reason_user'].label = "Justifier votre choix "
        self.fields['answer_justified_user'].required = True
        self.fields['likert_confidence'].required = True
        self.fields['reason_user'].required = True


class AlgocateFinalQuestionForm(ModelForm):
    class Meta:
        model = AlgocateFinalQuestion
        fields = ['useful', 'pros', 'cons', 'improvements', 'diff_ibex',]

    def __init__(self, *args, **kwargs):
        super(AlgocateFinalQuestionForm, self).__init__(*args, **kwargs)
        self.fields['useful'].label = "L’outil Algocate vous semble-t-il adapté pour faciliter la compréhension et l'acceptation d’un système algorithmique ?"
        self.fields['pros'].label = "Quels sont selon vous ses avantages (s’il y en a) ?"
        self.fields['cons'].label = "Quels sont selon vous ses inconvénients (s’il y en a) ?"
        self.fields['improvements'].label = "Voyez-vous des pistes d’amélioration ?"
        self.fields['diff_ibex'].label = "Quelles différences voyez-vous entre les explications fournies par IBEX et les justifications fournies par Algocate ? L’une des deux approches vous semble-t-elle préférable à l’autre ?"

