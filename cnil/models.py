from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator

from us_cnil.settings import *
from data.db_example import *

CHOICE_FEATURE = (
    # ('disbursed_amount', VAR_DICT_NAME['disbursed_amount']),
    ('asset_cost', VAR_DICT_NAME['asset_cost']),
    ('contrib_percentage', VAR_DICT_NAME['contrib_percentage']),
    ('employment_type', VAR_DICT_NAME['employment_type']),
    ('pri_no_of_accts', VAR_DICT_NAME['pri_no_of_accts']),
    ('pri_active_accts', VAR_DICT_NAME['pri_active_accts']),
    ('pri_overdue_accts', VAR_DICT_NAME['pri_overdue_accts']),
    ('pri_current_balance', VAR_DICT_NAME['pri_current_balance']),
    ('primary_instal_amt', VAR_DICT_NAME['primary_instal_amt']),
    ('new_accts_in_last_six_months', VAR_DICT_NAME['new_accts_in_last_six_months']),
    ('delinquent_accts_in_last_six_months', VAR_DICT_NAME['delinquent_accts_in_last_six_months']),
    ('average_acct_age', VAR_DICT_NAME['average_acct_age']),
    ('credit_history_length', VAR_DICT_NAME['credit_history_length']),
    ('no_of_inquiries', VAR_DICT_NAME['no_of_inquiries']),
    ('age', VAR_DICT_NAME['age']),
)
CHOICE_EMPLOYMENT_TYPE = (
    ("Self_employed", "À son compte"),
    ("Salaried", "Salarié"),
    ("None", "Non renseigné"),
)
CHOICES_DECISION = (
    ('1', 'Acceptée'),
    ('0', 'Refusée'),
)
CATE_OPERATOR = (
    ('', ''),
    ('est', 'est'),
    ("n'est pas", "n'est pas"),
)
INT_OPERATOR = (
    ('', ''),
    ('>=', '>='),
    ('<=', '<='),
)
TARGET_CHOICES = (
    ('1', '>50K'),
    ('0', '<=50K'),
)
SIMPLICITY_CHOICE = (
    ("1", "Plus complète"),
    ("2", "Plus simple"),
)
REALISM_CHOICE = (
    ("1", "Focus sur l'algorithme"),
    ("2", "Focus sur l'utilisation"),
)
FORMAT_CHOICE = (
    ("FImportanceExplanation", "Importance de variable"),
    ("RBMExplanation", "Règles"),
    ("CFactualExplanation", "Contrefactuelle"),
)
CHOICES_BOOL = (
    ('Oui', 'Oui'),
    ('Non', 'Non'),
)
LIKERT_CHOICES = (
    ('1', '1 (pas du tout confiant)'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5 (tout à fait confiant)'),
)
PROFILE_CHOICE = (
    ("Technique", "Technique"),
    ("Juridique", "Juridique"),
    ("Autre", "Autre"),
)
DEFAULT_USER = 0

BASE = LoanBase()
DATA = BASE.population
DATA['decision'] = BASE.model(DATA)
DATA['y_true'] = BASE.y_true
all_explanations = load('explanation/saved_explanations.joblib')
all_explanations.loc[:, 'simplicity'] = all_explanations['simplicity'].astype(int)
expl_sel = all_explanations[['explanation', 'realism', 'sample_id', 'simplicity']].drop_duplicates()
grouped = expl_sel.groupby('sample_id').count()['explanation']
sample_ids = grouped[grouped >= 12].index
EXPLANATIONS = all_explanations.loc[all_explanations.sample_id.isin(sample_ids) & all_explanations.sample_id.isin(DATA.index)]


class Subject(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=DEFAULT_USER)
    control_grp = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def progress_ibex(self):
        if self.user.decisionexample_set.count() < N_DECISION_EXAMPLES:
            return 'decision_example', self.user.decisionexample_set.count()
        if self.user.ibexexample_set.count() < N_IBEX_EXAMPLES:
            return 'ibex_example', self.user.ibexexample_set.count()
        if self.user.taskibex_set.count() < N_IBEX_TASKS:
            return 'ibex_task', self.user.taskibex_set.count()
        elif self.user.ibexfinalquestion_set.count() < 1:
            return 'ibex_final', 0
        else:
            return 'welcome_page'

    @property
    def progress_algocate(self):
        if self.user.decisionexample_set.count() < N_DECISION_EXAMPLES:
            return 'decision_example', self.user.decisionexample_set.count()
        if self.user.algocateexample_set.count() < N_ALGOCATE_EXAMPLES:
            return 'algocate_example', self.user.algocateexample_set.count()
        if self.user.taskalgocate_set.count() < N_ALGOCATE_TASKS:
            return 'algocate_task', self.user.taskalgocate_set.count()
        elif self.user.algocatefinalquestion_set.count() < 1:
            return 'algocate_final', 0
        else:
            return 'welcome_page'


class DecisionExample(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    case_id = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)


class IbexExample(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    case_id = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)


class TaskIbex(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=DEFAULT_USER)
    case_id = models.PositiveIntegerField(default=0)
    age_case = models.FloatField(default=0)
    main_factor_answer_user = models.CharField(choices=CHOICE_FEATURE, default=CHOICE_FEATURE[0], max_length=50, blank=True, null=True)
    main_factor_answer_true = models.CharField(choices=CHOICE_FEATURE, default=CHOICE_FEATURE[0], max_length=50)
    other_factors_answer_user = models.CharField(max_length=1000, default='', blank=True, null=True)
    other_factors_answer_true = models.CharField(max_length=1000, default='')
    extra_question = models.CharField(max_length=3000, default='', blank=True, null=True)
    extra_question_answer_user = models.BooleanField(default=True, blank=True, null=True)
    extra_question_answer_true = models.BooleanField(default=True, blank=True, null=True)
    explanation_user = models.TextField(default='', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class LoanFile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=DEFAULT_USER, blank=True, null=True)
    task_ibex = models.ForeignKey(TaskIbex, on_delete=models.CASCADE, blank=True, null=True)
    # disbursed_amount = models.IntegerField(verbose_name=VAR_DICT_NAME['disbursed_amount'])
    asset_cost = models.IntegerField(verbose_name=VAR_DICT_NAME['asset_cost'])
    contrib_percentage = models.FloatField(verbose_name=VAR_DICT_NAME['contrib_percentage'])
    employment_type = models.CharField(max_length=200,
                                       choices=CHOICE_EMPLOYMENT_TYPE,
                                       verbose_name=VAR_DICT_NAME['employment_type'],
                                       default=VAR_DICT_NAME['employment_type'][0])
    pri_no_of_accts = models.IntegerField(verbose_name=VAR_DICT_NAME['pri_no_of_accts'])
    pri_active_accts = models.IntegerField(verbose_name=VAR_DICT_NAME['pri_active_accts'])
    pri_overdue_accts = models.IntegerField(verbose_name=VAR_DICT_NAME['pri_overdue_accts'])
    pri_current_balance = models.IntegerField(verbose_name=VAR_DICT_NAME['pri_current_balance'])
    primary_instal_amt = models.IntegerField(verbose_name=VAR_DICT_NAME['primary_instal_amt'])
    new_accts_in_last_six_months = models.IntegerField(verbose_name=VAR_DICT_NAME['new_accts_in_last_six_months'])
    delinquent_accts_in_last_six_months = models.IntegerField(verbose_name=VAR_DICT_NAME['delinquent_accts_in_last_six_months'])
    average_acct_age = models.FloatField(verbose_name="Durée crédits (en cours) en années")
    credit_history_length = models.FloatField(verbose_name="Durée depuis premier crédit en années")
    no_of_inquiries = models.IntegerField(verbose_name=VAR_DICT_NAME['no_of_inquiries'])
    age = models.FloatField(verbose_name="Âge en années")
    created_at = models.DateTimeField(auto_now_add=True)


class IbexRequirement(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=DEFAULT_USER)
    task_ibex = models.ForeignKey(TaskIbex, on_delete=models.CASCADE, blank=True, null=True)
    simplicity = models.CharField(choices=SIMPLICITY_CHOICE, default=SIMPLICITY_CHOICE[0], max_length=1,
                                  help_text="Nombre d'éléments dans l'explication")
    realism = models.CharField(choices=REALISM_CHOICE, default=REALISM_CHOICE[0], max_length=1,
                               help_text="Prise en compte de la distribution réelle des variables")
    format = models.CharField(choices=FORMAT_CHOICE, default=FORMAT_CHOICE[0], max_length=23,
                              help_text="Type d'explication")
    created_at = models.DateTimeField(auto_now_add=True)


class IbexFinalQuestion(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=DEFAULT_USER)
    global_factors = models.TextField(default='', blank=True, null=True)
    sensible_factors = models.TextField(default='', blank=True, null=True)
    expected_behavior = models.TextField(default='', blank=True, null=True)
    next_steps = models.TextField(default='', blank=True, null=True)
    useful = models.TextField(default='', blank=True, null=True)
    pros = models.TextField(default='', blank=True, null=True)
    cons = models.TextField(default='', blank=True, null=True)
    improvements = models.TextField(default='', blank=True, null=True)
    profil = models.CharField(choices=PROFILE_CHOICE, default=PROFILE_CHOICE[0], max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class AlgocateExample(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    case_id = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class TaskAlgocate(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=DEFAULT_USER)
    case_id = models.PositiveIntegerField(default=0)
    answer_justified_user = models.CharField(choices=CHOICES_BOOL, max_length=10, blank=True, null=True)
    answer_justified_true = models.BooleanField(default=True)
    likert_confidence = models.CharField(choices=LIKERT_CHOICES, default='', max_length=10, blank=True, null=True)
    reason_user = models.TextField(default='', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Contest(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=DEFAULT_USER, blank=True, null=True)
    task_algocate = models.ForeignKey(TaskAlgocate, on_delete=models.CASCADE, blank=True, null=True)
    # disbursed_amount_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['disbursed_amount'], blank=True, null=True)
    # disbursed_amount_value = models.IntegerField(verbose_name='', blank=True, null=True)
    asset_cost_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['asset_cost'], blank=True, null=True)
    asset_cost_value = models.IntegerField(verbose_name='', blank=True, null=True)
    contrib_percentage_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['contrib_percentage'], blank=True, null=True)
    contrib_percentage_value = models.FloatField(verbose_name='', blank=True, null=True)
    employment_type_operator = models.CharField(max_length=99, choices=CATE_OPERATOR, verbose_name=VAR_DICT_NAME['employment_type'], blank=True, null=True)
    employment_type_value = models.CharField(max_length=200, choices=CHOICE_EMPLOYMENT_TYPE, verbose_name='', blank=True, null=True)
    pri_no_of_accts_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['pri_no_of_accts'], blank=True, null=True)
    pri_no_of_accts_value = models.IntegerField(verbose_name='', blank=True, null=True)
    pri_active_accts_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['pri_active_accts'], blank=True, null=True)
    pri_active_accts_value = models.IntegerField(verbose_name='', blank=True, null=True)
    pri_overdue_accts_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['pri_overdue_accts'], blank=True, null=True)
    pri_overdue_accts_value = models.IntegerField(verbose_name='', blank=True, null=True)
    pri_current_balance_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['pri_current_balance'], blank=True, null=True)
    pri_current_balance_value = models.IntegerField(verbose_name='', blank=True, null=True)
    primary_instal_amt_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['primary_instal_amt'], blank=True, null=True)
    primary_instal_amt_value = models.IntegerField(verbose_name='', blank=True, null=True)
    new_accts_in_last_six_months_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['new_accts_in_last_six_months'], blank=True, null=True)
    new_accts_in_last_six_months_value = models.IntegerField(verbose_name='', blank=True, null=True)
    delinquent_accts_in_last_six_months_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['delinquent_accts_in_last_six_months'], blank=True, null=True)
    delinquent_accts_in_last_six_months_value = models.IntegerField(verbose_name='', blank=True, null=True)
    average_acct_age_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['average_acct_age'], blank=True, null=True)
    average_acct_age_value = models.IntegerField(verbose_name='', blank=True, null=True)
    credit_history_length_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['credit_history_length'], blank=True, null=True)
    credit_history_length_value = models.IntegerField(verbose_name='', blank=True, null=True)
    no_of_inquiries_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['no_of_inquiries'], blank=True, null=True)
    no_of_inquiries_value = models.IntegerField(verbose_name='', blank=True, null=True)
    age_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name=VAR_DICT_NAME['age'], blank=True, null=True)
    age_value = models.IntegerField(verbose_name='', blank=True, null=True)

    # decision_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Décision", blank=True, null=True)
    y_true_value = models.CharField(max_length=100, choices=CHOICES_DECISION, verbose_name='', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class AlgocateFinalQuestion(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=DEFAULT_USER)
    useful = models.TextField(default='', blank=True, null=True)
    pros = models.TextField(default='', blank=True, null=True)
    cons = models.TextField(default='', blank=True, null=True)
    improvements = models.TextField(default='', blank=True, null=True)
    diff_ibex = models.TextField(default='', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class CommentFinal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    comment = models.CharField(max_length=30000, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


