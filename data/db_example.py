from data.db_type_definition import *
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn import tree
import pydotplus
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from joblib import dump, load


def show_tree(dt, features_name):
    dot_data = tree.export_graphviz(dt,
                                    feature_names=features_name,
                                    out_file=None,
                                    filled=True,
                                    rounded=True)
    graph = pydotplus.graph_from_dot_data(dot_data)
    graph.write_png('data/saved_models/decision_tree_adult.png')


class AdultBase(TabularBase):
    def __init__(self):
        self.categorical_features = [
            'sex', 'native_country', 'marital_status',
            'relationship', #'education'
        ]
        self.feature_names = [
            'age', 'education_num', # 'education',
            'marital_status',
            'relationship', 'sex', 'hours_per_week',
            'native_country', "fake_income"
        ]
        super(AdultBase, self).__init__()
        self.one_hot_encoder = self._train_one_hot_encoder()
        self.class_names = ['>=50k', '<50k']

    def __str__(self):
        return "adult"

    def load_data(self):
        data = pd.read_csv("./data/dataset/adult.csv")
        data.drop('fnlwgt', axis=1, inplace=True)
        data.drop('race', axis=1, inplace=True)
        # preprocess
        data.replace('?', np.nan, inplace=True)
        data.dropna(inplace=True)
        data.replace(['Divorced', 'Married-AF-spouse',
                      'Married-civ-spouse', 'Married-spouse-absent',
                      'Never-married', 'Separated', 'Widowed'],
                     ['divorced', 'married', 'married', 'married',
                      'not married', 'not married', 'not married'], inplace=True)
        data.reset_index(drop=True, inplace=True)
        data['Income'] = data['Income'].apply(lambda x: x.replace('.', ''))

        # create fake_income variable
        data['income'] = ((data['Income'] == '>50K') * 80000 | (data['Income'] == '<=50K') * 28085).astype(int)
        data['fake_income'] = data.groupby(['occupation', 'workclass']).transform('mean')['income']
        data['fake_income'] = data['fake_income'] * data['hours_per_week'] / data['hours_per_week'].mean()
        data['fake_income'] = data['fake_income'].astype(int)
        del data['income'], data['occupation'], data['workclass']

        # format output variable as a 0/1 output
        data['Income'] = (data['Income'] == '>50K').astype(int)
        x, y = data.drop('Income', axis=1), data['Income']

        return x[self.feature_names], y  # redefine column ordering to match the model ordering

    def load_model(self, model_path='./data/saved_models/decision_tree_adult.joblib'):
        # return lambda x: 1
        model = load(model_path)
        OHEncoder = load(os.path.join(BASE_DIR, ONE_HOT_ENCODE_PATH) + str(self) + ".joblib")
        pipe = Pipeline(steps=[('OHEncode', OHEncoder),
                               ('tree', model)])
        return lambda x: pipe.predict(x[self.feature_names])

    def display(self, samples):
        print(samples)

    def _train_and_save_model(self, model_path='data/saved_models/decision_tree_adult.joblib'):
        X = self.one_hot_encode(self.population[self.feature_names])

        X_train, X_test, y_train, y_test = train_test_split(X, self.y_true, test_size=.33)

        dt = DecisionTreeClassifier(max_leaf_nodes=20)
        dt.fit(X_train, y_train)
        print("Accuracy score (test dataset): ", accuracy_score(y_test, dt.predict(X_test)))

        show_tree(dt, X.columns)
        dump(dt, model_path)


class FicoBase(TabularBase):
    def __init__(self):
        self.categorical_features = [
            'State', 'Home.Ownership', 'Employment.Length', "Loan.Purpose",
        ]
        self.feature_names = [
            'Amount.Requested', 'Amount.Funded.By.Investors',
            'Interest.Rate', 'Loan.Length', 'Loan.Purpose',
            'Debt.To.Income.Ratio', 'State', 'Home.Ownership',
            'Monthly.Income', 'Open.CREDIT.Lines', 'Revolving.CREDIT.Balance',
            'Inquiries.in.the.Last.6.Months', 'Employment.Length'
        ]
        super(FicoBase, self).__init__()
        self.one_hot_encoder = self._train_one_hot_encoder()
        self.class_names = ['bad', 'good']
        self.decoded_model = self.load_decoded_model()
        self.std = self.compute_std()

    def __str__(self):
        return "fico"

    def load_data(self):
        data = pd.read_csv(
            "./data/dataset/loansData.csv",
            # nrows=5000,
        )
        data.dropna(inplace=True)
        data['Interest.Rate'] = data['Interest.Rate'].str.replace('%', '').astype(float)
        data['Debt.To.Income.Ratio'] = data['Debt.To.Income.Ratio'].str.replace('%', '').astype(float)
        data['Loan.Length'] = data['Loan.Length'].str.replace(' months', '').astype(int)
        data['FICO.Range'] = data['FICO.Range'].apply(lambda x: (int(x.split('-')[0]) + int(x.split('-')[0])) / 2.)
        data['output'] = (data['FICO.Range'] >= 700)
        del data['FICO.Range']
        x, y = data.drop('output', axis=1), data['output']

        return x[self.feature_names], y  # redefine column ordering to match the model ordering

    def load_model(self, model_path='./data/saved_models/decision_tree_fico.joblib'):
        # return lambda x: 1
        model = load(model_path)
        OHEncoder = load(os.path.join(BASE_DIR, ONE_HOT_ENCODE_PATH) + str(self) + ".joblib")
        pipe = Pipeline(steps=[('OHEncode', OHEncoder),
                               ('tree', model)])
        return lambda x: pipe.predict(x[self.feature_names])

    def load_decoded_model(self, model_path='./data/saved_models/decision_tree_fico.joblib'):
        model = load(model_path)
        pipe = Pipeline(steps=[('tree', model)])
        return lambda x: pipe.predict(x[self.encoded_feature_names])

    def display(self, samples):
        print(samples)

    def _train_and_save_model(self, model_path='data/saved_models/decision_tree_fico.joblib'):
        X = self.one_hot_encode(self.population[self.feature_names])

        X_train, X_test, y_train, y_test = train_test_split(X, self.y_true, test_size=.33)

        dt = DecisionTreeClassifier(max_leaf_nodes=20)
        dt.fit(X_train, y_train)
        print("Accuracy score (test dataset): ", accuracy_score(y_test, dt.predict(X_test)))

        show_tree(dt, X.columns)
        dump(dt, model_path)


class LoanBase(TabularBase):
    def __init__(self):
        self.categorical_features = [
            'employment_type'
        ]
        self.feature_names = [
            #'disbursed_amount',
            'asset_cost', 'contrib_percentage', 'employment_type',
            'pri_no_of_accts', 'pri_active_accts', 'pri_overdue_accts',
            'pri_current_balance', 'primary_instal_amt',
            'new_accts_in_last_six_months', 'delinquent_accts_in_last_six_months',
            'average_acct_age', 'credit_history_length', 'no_of_inquiries',
            'age',
            # 'pan_flag', 'voterid_flag',
        ]
        # write custom rules by increasing priority
        self.custom_rules = [
            {
            'name': 'règle du premier crédit',
            'description': "politique de favorisation de premier crédit pour les faibles montants",
            'input_conditions': [('pri_no_of_accts',  ' <= ', ' 0 '), ('asset_cost',  ' <= ', ' 55000 ')],
            'output_val': 1,
        },
            {
            'name': 'règle des deux défauts dans les 6 mois',
            'description': "politique de refus systèmatique pour les clients deux fois en défaut dans les six derniers mois",
            'input_conditions': [('delinquent_accts_in_last_six_months',  ' >= ', ' 2 ')],
            'output_val': 0,
        },
            {
            'name': 'discriminatory_rule',
            'description': " Règle discriminatoire à l'encontre des 60 ans et plus  ",
            'input_conditions': [('age',  ' >= ', ' 60 '), ],
            'output_val': 0,
        },]
        super(LoanBase, self).__init__()
        self.one_hot_encoder = self._train_one_hot_encoder()
        self.one_hot_encode(self.population)
        self.class_names = ['bad', 'good']
        self.decoded_model = self.load_decoded_model()
        self.std = self.compute_std()

    def __str__(self):
        return "loan"

    def load_data(self):
        df = pd.read_csv("./data/dataset/balanced_loan_data_ready.csv")
        x, y = df.drop('decision', axis=1), df['decision']
        return x[self.feature_names], y  # redefine column ordering to match the model ordering

    def load_data_(self):
        def correct_date(date_str):
            if int(date_str.split('-')[2]) < 20:
                return date_str[0:6] + '20' + date_str[-2:]
            else:
                return date_str[0:6] + '19' + date_str[-2:]

        df = pd.read_csv(
            "./data/dataset/train.csv",
        )
        # df = df.head(20000)
        df = df.drop([
            'UniqueID', 'branch_id', 'supplier_id', 'Current_pincode_ID',
            'State_ID', 'Employee_code_ID', 'manufacturer_id',
            'PRI.SANCTIONED.AMOUNT', 'PRI.DISBURSED.AMOUNT',
            'Aadhar_flag', 'MobileNo_Avl_Flag', 'Driving_flag', 'Passport_flag',
            'PAN_flag', 'VoterID_flag',
        ], axis=1)
        df.columns = df.columns.str.replace('.', '_').str.lower()
        df = df[[col for col in df.columns if 'sec_' not in col and '_cns_' not in col]]
        df.loc[:, 'date_of_birth'] = pd.to_datetime(df['date_of_birth'].apply(correct_date))
        df['age'] = (dt.datetime.now() - df.loc[:, 'date_of_birth']).apply(lambda x: int(x.days) / 365.25)
        del df['date_of_birth']
        df.loc[:, 'disbursaldate'] = pd.to_datetime(df['disbursaldate'])
        del df['disbursaldate']
        df.loc[:, 'employment_type'] = df['employment_type'].str.replace(' ', '_')
        df.loc[df['employment_type'].isnull(), 'employment_type'] = 'None'
        df['average_acct_age'] = df['average_acct_age'].apply(lambda x: x[:-3].split('yrs')).apply(
            lambda x: int(x[0]) + int(x[1]) / 12.)
        df['credit_history_length'] = df['credit_history_length'].apply(lambda x: x[:-3].split('yrs')).apply(
            lambda x: int(x[0]) + int(x[1]) / 12.)
        df.loc[:, 'contrib_percentage'] = 100 - df['ltv']
        del df['ltv']
        x, y = df.drop('loan_default', axis=1), 1 - df['loan_default']

        return x[self.feature_names], y  # redefine column ordering to match the model ordering

    def load_model(self, model_path='./data/saved_models/decision_tree_loan.joblib'):
        model = load(model_path)
        OHEncoder = load(os.path.join(BASE_DIR, ONE_HOT_ENCODE_PATH) + str(self) + ".joblib")
        pipe = Pipeline(steps=[('OHEncode', OHEncoder),
                               ('tree', model)])

        def model(x):
            pred = pipe.predict(x[self.feature_names])
            for rule in self.custom_rules:
                cdt = rule['input_conditions']
                mask = x.eval(' and '.join([''.join(predicate) for predicate in cdt]))
                pred[mask] = rule['output_val']
            return pred
        return model

    def load_decoded_model(self, model_path='./data/saved_models/decision_tree_loan.joblib'):
        model = load(model_path)
        pipe = Pipeline(steps=[('tree', model)])
        return lambda x: pipe.predict(x[self.encoded_feature_names])

    def display(self, samples):
        print(samples)

    def train_and_save_model_(self, model_path='data/saved_models/decision_tree_loan.joblib'):
        x, y = self.load_data_()
        X = self.one_hot_encode(x[self.feature_names])

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.33)

        dt = DecisionTreeClassifier(max_leaf_nodes=20, class_weight='balanced')
        dt.fit(X_train, y_train)
        print("Accuracy score (test dataset): ", accuracy_score(y_test, dt.predict(X_test)))
        # keep only samples that are correctly predicted
        print("Dataset size: ", len(x))
        mask = (y == dt.predict(X))
        x = x.loc[mask]
        y = y[mask]
        print("Removing ", sum(1 - mask), "samples incorrectly predicted")
        print("Dataset size: ", len(x))
        # balance final dataset (len(accepted) = len(refused))
        x.loc[:, 'decision'] = y
        print("Size decision 1", sum(x['decision'] == 1), "Size decision 0", sum(x['decision'] == 0))
        remove_size = int((sum(x['decision'] == 1) - sum(x['decision'] == 0)) * .7)
        drop_indices = x[(x['decision'] == 1)].sample(remove_size).index
        x = x.drop(drop_indices)
        print("Removing ", remove_size, "decision 1 samples to balance the classes")
        print("Dataset size: ", len(x))
        # save final dataset
        x.to_csv("data/dataset/balanced_loan_data_ready.csv", index=False)
        show_tree(dt, X.columns)
        dump(dt, model_path)
