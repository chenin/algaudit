import numpy as np
import datetime as dt
import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder
from shared.distance import tabular_feature_similarity_to_x
import os
from us_cnil.settings import *

from settings import *
from joblib import dump, load

cfg_img = cfg['base']['image']

ONE_HOT_ENCODE_PATH = "data/saved_models/one_hot_encoder_"

class BaseXAI():
    def __init__(self):
        self.x_raw, self.y_true = self.load_data()
        self.model = self.load_model()
        self.y_raw = self.model(self.x_raw)
        self.scope_type = None
        self.encoded_feature_names = []
        self.categorical = None
        self.scope_pred = None
        self.regression = True
        self.classification = False

    def init_scope(self, how='random'):
        if how == 'all':
            self.scope = self.population
            self.scope_type = 'global'
        else:
            if type(how)==int:
                self.scope = self.population.loc[how:how]
            if how == 'random':
                self.scope = self.population.sample(1)
            self.scope_type = 'local'
            self.scope_pred = self.model(self.scope)


class TabularBase(BaseXAI):
    def __init__(self):
        BaseXAI.__init__(self)
        self.data_type = 'tabular'
        self.population = self.x_raw.copy()
        if getattr(self, "categorical_features", None):
            self.categorical = True
        else:
            self.categorical = False
            self.min_values = self.compute_min()
            self.max_values = self.compute_max()
        try:
            self.feature_names
        except:
            self.feature_names = [str(x) for x in self.x_raw.columns]
        self.one_hot_encoder = None
        self.y_range = self.compute_range_wo_outliers(self.population)

    def pop_similarity_to_x(self, x, features_subset=[]):
        dist_df = pd.DataFrame()
        # define the list of columns that are used to compute the similarity
        if len(features_subset) > 0:
            col_list = features_subset
        else:
            col_list = self.population.columns
        # compute column-wise similarity
        for col in col_list:
            dist_df[col] = tabular_feature_similarity_to_x(self.population[col],
                                                           x[col].values[0],
                                                           self.std.get(col, None))
        return dist_df.mean(axis=1)

    def compute_range_wo_outliers(self, data):
        y_range_wo_outiles = {}
        for col in data.columns:
            try:
                y_range_wo_outiles[col] = 0, data[col].quantile(0.98)
            except:
                None
        return y_range_wo_outiles

    def compute_std(self):
        data_copy = self.one_hot_encode(self.x_raw).copy()
        for col in data_copy.columns:
            if type(data_copy[col].head(1).values[0]) in [dt.date]:
                data_copy.loc[:, col] = data_copy[col] - dt.date(2000, 1, 1)
        return data_copy.std()

    def compute_min(self):
        return self.x_raw.min()

    def compute_max(self):
        return self.x_raw.max()

    def load_one_hot_encoder(self):
        try:
            preprocessor = load(os.path.join(BASE_DIR, ONE_HOT_ENCODE_PATH) + str(self) + ".joblib")
            if not preprocessor.get_params()['transformers'][0][2] == self.categorical_features:
                preprocessor = self._train_one_hot_encoder()
        except FileNotFoundError:
            preprocessor = self._train_one_hot_encoder()
        return preprocessor

    def _train_one_hot_encoder(self):
        # encoding categorical features for the decision tree
        non_cate_features = [x for x in self.feature_names if x not in self.categorical_features]
        categorical_transformer = Pipeline(steps=[('onehot', OneHotEncoder(handle_unknown='ignore'))])
        preprocessor = ColumnTransformer(
            transformers=[('cat', categorical_transformer, self.categorical_features)],
            remainder='passthrough',
            sparse_threshold=0,
        )
        preprocessor.fit(self.population[self.feature_names])
        dump(preprocessor, os.path.join(BASE_DIR, ONE_HOT_ENCODE_PATH) + str(self) + ".joblib")
        return preprocessor

    def one_hot_encode(self, samples):
        """
        Apply learned one hot encoding. Need to account for samples that are subset of all columns.
        """
        if len(samples.columns) < len(self.feature_names):
            extra_cols = self.population[[x for x in self.feature_names if x not in samples.columns]]
            extra_cols = extra_cols.sample(len(samples)).copy()
            extra_cols.set_index(samples.index, inplace=True)
            full_col_samples = pd.concat([samples, extra_cols], axis=1)[self.feature_names]
            col_augmented = True
        else:
            col_augmented = False
            full_col_samples = samples[self.feature_names]
        X = self.one_hot_encoder.transform(full_col_samples)
        cate_fnames = self.one_hot_encoder.transformers_[0][1]['onehot'].get_feature_names()

        # put old column name in the encoded feature name
        cate_fname_full = [self.categorical_features[int(f.split('_')[0][1:])] + '__is__' + f[3:] for f in cate_fnames]
        non_cate_features = [x for x in self.feature_names if x not in self.categorical_features]
        col_names = list(cate_fname_full) + list(non_cate_features)
        encoded_samples = pd.DataFrame(data=X, columns=col_names)
        self.encoded_feature_names = encoded_samples.columns
        self.encoded_names_dict = {
            k: list(filter(lambda x: x.split('__')[0] == k, self.encoded_feature_names))
            for k in self.feature_names
        }
        if col_augmented:
            encoded_samples = encoded_samples[
                [x for x in encoded_samples.columns if x in samples.columns or x.split('__')[0] in samples.columns]
            ]
        return encoded_samples
