import numpy as np

from us_cnil.settings import *
from cnil.utils import *

def final_norm_answer(ttest_contest, ttest_rbm, ttest_rbm_neg):
    contest_bool = np.sign(ttest_contest) * (abs(ttest_contest) > TTEST_THRESHOLD)
    rbm_bool = ttest_rbm > TTEST_THRESHOLD
    rbm_neg_bool = ttest_rbm_neg < - TTEST_THRESHOLD

    if rbm_bool and rbm_neg_bool:
        if abs(ttest_rbm) > abs(ttest_rbm_neg):
            return "M1"
        else:
            return "M0"

    if not rbm_bool and not rbm_neg_bool:
        if not contest_bool:
            return "M"
        elif ttest_contest > 0:
            return "1"
        elif ttest_contest < 0:
            return "0"

    if contest_bool > 0 and rbm_bool and not rbm_neg_bool:
        return "1"

    if contest_bool > 0 and not rbm_bool and rbm_neg_bool:
        if abs(ttest_contest) > abs(ttest_rbm_neg):
            return "1"
        else:
            return "M0"

    if contest_bool == 0 and rbm_bool and not rbm_neg_bool:
        return "M1"

    if contest_bool == 0 and not rbm_bool and rbm_neg_bool:
        return "M0"

    if contest_bool <= 0 and rbm_bool and not rbm_neg_bool:
        if abs(ttest_contest) > abs(ttest_rbm):
            return "0"
        else:
            return "M1"

    if contest_bool <= 0 and not rbm_bool and rbm_neg_bool:
        return "0"


def justification_formatting(all_norms_dict, target_output, cv_predicates):
    """

    :param all_norms_dict: list of norm info dictionnaries
    [{'name': ..., 'value': ..., 'avg': ..., 'size': ... }, ... ]
    :return: html code to be displayed
    """
    text = "Vous pensez que la demande devrait être {decision} car {cv_predicates}. "
    text += '<br><br>'
    text = text.format(
        decision=target_output,
        cv_predicates=cv_predicates,
    )
    for norm_info in all_norms_dict:
        if norm_info['type'] in ['objective', 'reference']:
            ttest_contest = norm_info['ttest']
            ttest_rbm = norm_info['rbm_ttest']
            ttest_rbm_neg = norm_info['rbm_neg_ttest']
            answer = final_norm_answer(ttest_contest, ttest_rbm, ttest_rbm_neg)
            if answer == "M":
                text += "Aucune norme ne permet de conclure dans ces conditions  "
                text += "pour juger de cette décision. Pour tenter d'avoir une réponse concluante,"
                text += "vous pouvez essayer de réduire les conditions de votre affirmation en enlevant certaines conditions. "
            else:
                if "M" in answer:
                    mitigated = True
                    final_answer = answer[1]
                else:
                    mitigated = False
                    final_answer = answer
                text += "Globalement, la norme {name} tend à justifier que la demande soit "
                text += CLASS_NAMES[int(final_answer)] + ". <br>"
                text += "En effet, dans l'historique des demandes, les {size} dossiers tels que "
                text += "{cv_predicates} ont un {description} égal à {value} (moyenne générale: {avg}). "
                if abs(ttest_contest) < 50:
                    text += "Toutefois, cet écart à la moyenne n'est pas significatif. "
                if mitigated:
                    subtext = "Dans votre cas, Algocate a considéré également le fait que "
                    subtext += "les {size_rbm} dossiers tels que "
                    subtext += "{predicates_rbm} ont un {description} égal à {value_rbm} (moyenne générale: {avg}). "
                    subtext += "Ce qui a conduit à la conclusion finale. "
                    if final_answer == '1':
                        predicates_rbm = norm_info['rbm_predicate']
                        size_rbm = norm_info['rbm_size']
                        value_rbm = norm_info['rbm_value']
                    elif final_answer == "0":
                        predicates_rbm = norm_info['rbm_neg_predicate']
                        size_rbm = norm_info['rbm_neg_size']
                        value_rbm = norm_info['rbm_neg_value']
                    else:
                        raise ValueError("final_answer should be '0' or '1', not " + str(final_answer))
                    subtext = subtext.format(
                        size_rbm=size_rbm,
                        predicates_rbm=predicates_rbm,
                        value_rbm=float_to_str_percent(-value_rbm),
                        description=norm_info['description'],
                        avg=float_to_str_percent(-norm_info['avg']),
                    )
                    text += subtext
            text += '<br><br>'
            text = text.format(
                name=norm_info['name'],
                size=norm_info['size'],
                cv_predicates=cv_predicates,
                description=norm_info['description'],
                value=float_to_str_percent(-norm_info['value']),
                avg=float_to_str_percent(-norm_info['avg']),
            )
        elif norm_info['type'] == 'rule':
            if norm_info['test']:
                text += "La norme {name} de {description}"
                text += " s'applique au cas de l'intéressé et justifie "
                text += "que la demandé soit {output_rule}. "
                text = text.format(
                    description=norm_info['description'],
                    name=norm_info['name'],
                    output_rule=CLASS_NAMES[norm_info['output_value']]
                )
                text += '<br><br>'
                break
        else:
            raise ValueError("Norm type should be objective, reference or rule, not " + str(norm_info['type']))
    return text



