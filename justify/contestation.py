from generation.utils import *
from collections import namedtuple
import operator
import numpy as np


class Rule:
    def __init__(self, predicate_list):
        """
        Initialize the rule as a list of predicates
        :param predicate_list: list of tuples [(var_index, operator, var_index), , ...]
        """
        Predicate = namedtuple("Predicate", "l_index operator r_index")
        self.predicate_list = [Predicate(*pred) for pred in predicate_list]

    def test(self, x_l, x_r, suffixes=('', '')):
        return np.array(
            [pred.operator(x_l[pred.l_index + suffixes[0]], x_r[pred.r_index + suffixes[1]])
             for pred in self.predicate_list]
        ).all(axis=0)

    def select_equal_predicates(self):
        return [pred for pred in self.predicate_list if pred.operator == operator.eq]

    def select_non_equal_predicates(self):
        return [pred for pred in self.predicate_list if pred.operator != operator.eq]


class ContestVal:
    def __init__(self, predicate_list, target_predicate):
        self.type = 'values'
        self.predicate_list = predicate_list
        self.target_predicate = target_predicate
        formated_pred_list = [(it[0], it[1], it[0]) for it in self.predicate_list]
        self.values = {it[0]: it[2] for it in self.predicate_list}
        self.rule = Rule(formated_pred_list)
        self.target_rule_obj = Rule([(target_predicate[0], target_predicate[1], target_predicate[0])])
        self.target_rule_func = lambda x: self.target_rule_obj.test(x, {target_predicate[0]: target_predicate[2]})

    def test_antecedent(self, x):
        return self.rule.test(x, self.values)

    def test_consequent(self, x, negative=False):
        if negative:
            return ~self.target_rule_func(x)
        else:
            return self.target_rule_func(x)

    def test(self, x, negative=False):
        return self.test_antecedent(x) & self.test_consequent(x, negative=negative)


class ContestRel:
    def __init__(self, predicate_list, target_rule_str):
        self.type = 'relative'
        self.target_rule_str = target_rule_str
        self.target_rule_func = str_rule_to_filter(self.target_rule_str)
        self.rule = Rule(predicate_list)

    def test_antecedent(self, x_l, x_r, suffixes=('', '')):
        feature_test = self.rule.test(x_l, x_r, suffixes=suffixes)
        output_test = self.target_rule_func(x_r)
        return feature_test & output_test

    def test_consequent(self, x_l, x_r, negative=False):
        if negative:
            return ~(self.target_rule_func(x_l) & self.target_rule_func(x_r))
        else:
            return self.target_rule_func(x_l) & self.target_rule_func(x_r)

    def test(self, x_l, x_r, negative=False, suffixes=('', '')):
        return (
            self.test_antecedent(x_l, x_r, suffixes=suffixes)
            &
            self.test_consequent(x_l, x_r, negative=negative)
            )
