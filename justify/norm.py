from abc import ABCMeta, abstractmethod
from scipy import stats

from cnil.models import *

class Norm():
    __metaclass__ = ABCMeta
    def __init__(self):
        self.type = "objective"

    @abstractmethod
    def _eval(self, undetermined_contest_df, contest=None):
        raise NotImplementedError("Must override _eval")

    @abstractmethod
    def eval(self, undetermined_contest_df, contest=None):
        raise NotImplementedError("Must override eval")

    def average(self, contest, data):
        # filtered_data = data.loc[contest.target_rule_func(data)]
        return round(self._eval(data, contest).mean(), 3)

    def value(self, contest, undetermined_contest_df):
        # filtered_data = data.loc[contest.target_rule_func(data)]
        return round(self._eval(undetermined_contest_df, contest).mean(), 3)

    def ttest(self, contest, data, undetermined_contest_df):
        # filtered_data = data.loc[contest.target_rule_func(data)]
        ttest = stats.ttest_ind(self._eval(undetermined_contest_df, contest), self._eval(data, contest), equal_var=False)
        return (ttest.statistic, ttest.pvalue)

    def evidence(self, contest, data, undetermined_contest_df):
        # filtered_data = data.loc[contest.target_rule_func(data)]
        ttest = stats.ttest_ind(self._eval(undetermined_contest_df, contest), self._eval(data, contest), equal_var=False)
        evidence = {
            'average': self.average(contest, data),
            'value': self.value(contest, undetermined_contest_df),
            'size': len(undetermined_contest_df),
            'ttest': ttest.statistic,
            'pvalue': ttest.pvalue,
        }
        return evidence


class YouthNorm(Norm):
    def __init__(self):
        super(YouthNorm, self).__init__()
        self.description = "âge moyen"
        self.type = "objective"

    def __str__(self):
        return "objectif priorité aux jeunes"

    def _eval(self, undetermined_contest_df, contest=None):
        return - undetermined_contest_df['age']

    def eval(self, undetermined_contest_df, contest=None):
        return - undetermined_contest_df['age'].mean()


class ReferenceNorm(Norm):
    def __init__(self):
        super(ReferenceNorm, self).__init__()
        self.description = "taux moyen de défaut"
        self.type = "objective"

    def __str__(self):
        return "<b>NORME 3</b> (objectif de minimisation des risques de défaut)"

    def _eval(self, undetermined_contest_df, contest=None):
        return -(1 - contest.test_consequent(undetermined_contest_df))

    def eval(self, undetermined_contest_df, contest=None):
        return -(1 - contest.test_consequent(undetermined_contest_df).mean())


class CustomRule1():
    def __init__(self):
        self.rule_info = BASE.custom_rules[0]
        self.rules = self.rule_info['input_conditions']
        self.description = self.rule_info['description']
        self.short_name = self.rule_info['name']
        self.type = "rule"
        self.output_value = self.rule_info['output_val']

    def __str__(self):
        return "<b>NORME 2</b> (" + self.rule_info['name'] + ")"

    def test(self, scope):
        return scope.eval(' and '.join([''.join(predicate) for predicate in self.rules])).values[0]


class CustomRule2():
    def __init__(self):
        self.rule_info = BASE.custom_rules[1]
        self.rules = self.rule_info['input_conditions']
        self.description = self.rule_info['description']
        self.short_name = self.rule_info['name']
        self.type = "rule"
        self.output_value = self.rule_info['output_val']

    def __str__(self):
        return "<b>NORME 1</b> (" + self.rule_info['name'] + ")"

    def test(self, scope):
        return scope.eval(' and '.join([''.join(predicate) for predicate in self.rules])).values[0]


