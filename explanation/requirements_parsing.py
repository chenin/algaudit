import itertools

from explanation.explanation_placeholder import *

_SIMPLE = {
    1: {'n_items_max' : ["L"]},
    2: {'n_items_max': ["M"]},
    3: {'n_items_max': ["S"]},
}

_GENERAL = {
    3: {'sampling_scope': ["L"]},
    2: {'sampling_scope': ["M"]},
    1: {'sampling_scope': ["S"]},
}

_ACTIONABLE = {
    True: {'actionability': [True]},
    False: {'actionability': [False]},
}

_REALISTIC = {
    1: {'sampling' : [add_random_noise], 'realism': [1]},
    2: {'sampling': [perturb_one_sample_w_pop], 'realism': [2]},
    3: {'sampling': [select_closest], 'realism': [3]},
}

def req_to_param(base, requirement_set):
    param_set = {}
    param_set['format'] = [requirement_set['format']]

    # refer to dicionnary to parse argument for lisibility
    simple_params = copy.deepcopy(_SIMPLE[requirement_set['simplicity']])
    general_param = copy.deepcopy(_GENERAL[requirement_set['generality']])
    realistic_params = copy.deepcopy(_REALISTIC[requirement_set['realism']])

    param_set = {**param_set, **simple_params,
                 **general_param, **realistic_params}

    # transform list of param into list of param set, except for constraints
    param_set_list = [
        {list(param_set.keys())[it]: comb[it] for it in range(len(comb))}
        for comb in itertools.product(*param_set.values())
    ]
    return (*param_set_list, )


def display_requirements(requirements):
    hard_requirements = [req for req, v in requirements.items() if type(v) != list]
    soft_requirements = [req for req, v in requirements.items() if type(v) == list]
    print('HARD REQUIREMENTS:')
    for req in hard_requirements:
        print('\t', req, ':', requirements[req])
    print('SOFT REQUIREMENTS: ')
    for req in soft_requirements:
        if type(requirements[req][0]) == type:
            expl_names = {RBMExplanation: 'RB', DTExplanation: 'DT', FImportanceExplanation: 'LA',
                          PartialDepExplanation: 'PD', PearsonCoefExplanation: 'PC',
                          CFactualExplanation: 'CF'}
            print('\t', req, ':', ' > '.join([expl_names[v] for v in requirements[req]]))
        else:
            print('\t', req, ':', ' > '.join([str(v) for v in requirements[req]]))
