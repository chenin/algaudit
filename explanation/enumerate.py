from explanation.requirements_parsing import *
from explanation.explanation_placeholder import *
from shared.utils import *


ALL_REQUIREMENTS = {
    "format": [RBMExplanation, CFactualExplanation, FImportanceExplanation],
    "nature": [False],
    "simplicity": [1, 3],
    "generality": [3, 1],
    "realism": [3, 1],
    "actionability": [True]
}



def select_explanation(base, requirements, actionable_features=[]):
    comparative_table = enumerate_explanations(base, requirements, actionable_features=actionable_features)
    assert len(comparative_table)>0, "No explanation generated. "
    print(len(comparative_table), " explanations have been generated. ")
    # filter explanations that does not match hard constraints
    filtering_req = [req for req, v in requirements.items() if type(v) != list]
    for req in filtering_req:
        if base.scope_type == 'global' and req in ['generality', 'realism']:
            None
        else:
            f = (comparative_table[req] == requirements[req])
            comparative_table = comparative_table.loc[f]
    # if no explanation match the conditions, relaxe the requirements
    if len(comparative_table)==0:
        print("Relaxing requirement :", filtering_req[-1])
        del requirements[filtering_req[-1]]
        return select_explanation(base, requirements, actionable_features=actionable_features)
    else:
        # ordering remaining explanation by preferences
        selective_req = [req for req, v in requirements.items() if type(v) == list]
        if len(selective_req) > 0:
            for req in selective_req:
                ordering = {r: it for it, r in enumerate(requirements[req])}
                comparative_table[req + "__order"] = comparative_table[req].apply(lambda x: ordering[x])
            comparative_table.sort_values([req + '__order' for req in selective_req])
        return comparative_table.head(1)['explanation'].values[0]


def enumerate_explanations(base, requirements, actionable_features=[]):
    comparative_table = pd.DataFrame()
    all_parameters = enumerate_parameters(base, requirements)
    list_of_explanations = []
    for expl_param in all_parameters:
        explainer_obj = expl_param['format']
        explanation = explainer_obj(base, **expl_param, actionable_features=actionable_features)
        explanation.compute()
        if explanation.generated_bool:
            list_of_explanations.append(explanation)
            explanation.estimate_requirements(requirements.keys())
            comparative_table = comparative_table.append({c: getattr(explanation, c) for c in requirements.keys()}, ignore_index=True)
    comparative_table['explanation'] = list_of_explanations
    return comparative_table


def explanation_from_requirements(base, requirement_set, actionable_features=[]):
    explainer_obj = requirement_set['format']
    tech_params = req_to_param(base, requirement_set)[0]
    explanation = explainer_obj(base, **tech_params, actionable_features=actionable_features)
    explanation.compute()
    return explanation


def enumerate_parameters(base, requirements):
    research_space = ALL_REQUIREMENTS.copy()
    for k, v in requirements.items():
        if type(v)==list:
            research_space[k] = v
        else:
            research_space[k] = [v]
    req_enumerate = [
        {list(research_space.keys())[it]: comb[it] for it in range(len(comb))}
        for comb in itertools.product(*research_space.values())
    ]
    param_enum = [req_to_param(base, req_set) for req_set in req_enumerate]
    return flatten_array(param_enum)