from shared.distance import *
from shared.utils import *
from settings import *
import pdb

sampling_conf = cfg['sampling']


def identity_sampling(base, actionable_features=[]):
    samples = base.population.copy()
    samples['output'] = base.model(samples)
    if len(actionable_features) > 0:
        return samples[actionable_features + ['output']]
    else:
        return samples


def select_closest(base, scope, size=sampling_conf['select_closest']['size'],
                   convert_to_binary_classification=cfg['sampling']['convert_to_binary_classification'],
                   actionable_features=[]):
    similarities = base.pop_similarity_to_x(scope)
    if type(size) == float:
        n_closest = round(len(base.population) * size)
    elif type(size) == int:
        n_closest = size
    pop_copy = base.population.copy()
    pop_copy['simx'] = similarities
    pop_copy.sort_values('simx', ascending=True, inplace=True)
    samples = pop_copy.tail(n_closest +1 ).head(n_closest).drop('simx', axis=1)
    samples['output'] = base.model(samples)
    if len(actionable_features) > 0:
        return samples[actionable_features + ['output']]
    else:
        return samples
