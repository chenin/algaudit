import pandas as pd
import numpy as np
import pylab as plt

from data.db_example import *


def rdn_samples(mnist_base, sample, size, binary):
    chromozomes = pd.concat([sample] * size, axis=0)
    non_empty_avg = mnist_base.x_raw.mean()
    n_cols = len(chromozomes.columns)
    if mnist_base.representation_type == 'blur':
        rdm_data = np.random.rand(size, n_cols) * 10
        proba = [1 - non_empty_avg, non_empty_avg]
        rand_indices = np.random.choice([True, False], size=(size, n_cols), p=proba)
        rdm_data[rand_indices] = 0
    elif binary:
        proba = [non_empty_avg / 2., non_empty_avg / 2., 1 - non_empty_avg]
        rdm_data = np.random.choice([0, 1, np.nan], size=(size, n_cols), p=proba)
    else:
        rdm_data = np.random.rand(size, n_cols)
        proba = [1 - non_empty_avg, non_empty_avg]
        rand_indices = np.random.choice([True, False], size=(size, n_cols), p=proba)
        rdm_data[rand_indices] = np.nan

    chromozomes = pd.DataFrame(
        data=rdm_data,
        index=chromozomes.index,
        columns=chromozomes.columns
    )
    return chromozomes


def update_population(mnist_base, sample, children, chromozomes, new_size, binary, how='steady_state'):
    save_index = chromozomes.index[0]
    chromozomes.index = range(len(chromozomes))
    new_chromo = rdn_samples(mnist_base, sample, new_size, binary)
    if how == 'steady_state':
        chromozomes.drop(chromozomes.head(len(children) + new_size).index, inplace=True)
        chromozomes.index = [save_index] * len(chromozomes)
        del chromozomes['fitness'], chromozomes['rank']
        return pd.concat([chromozomes, children, new_chromo], axis=0, sort=False)
    elif how == 'elitism':
        elit = chromozomes.tail(len(children))
        drop_indices = np.random.choice(chromozomes.index, 2 * len(children) + new_size, replace=False)
        chromozomes.drop(drop_indices, inplace=True)
        chromozomes.index = [save_index] * len(chromozomes)
        elit.index = [save_index] * len(elit)
        del chromozomes['fitness'], chromozomes['rank'], elit['fitness'], elit['rank']
        return pd.concat([chromozomes, children, new_chromo, elit], axis=0, sort=False)


def mutate(chromo, mnist_base, sample, binary):
    selected = np.random.rand(len(chromo), len(chromo.columns)) < (1 / len(chromo.columns))
    non_empty_avg = mnist_base.x_raw.mean()
    proba = [non_empty_avg / 2., non_empty_avg / 2., 1 - non_empty_avg]
    noise = np.random.choice([0, 1, np.nan], size=(len(chromo), len(chromo.columns)), p=proba)
    noise = np.array(rdn_samples(mnist_base, sample, len(chromo), binary).values)
    data = np.array(chromo.values)
    data[np.array(selected)] = noise[np.array(selected)]
    chromo = pd.DataFrame(data=data, index=chromo.index, columns=chromo.columns)
    return chromo


def crossover(selected, population):
    n_cols = len(selected.columns)
    n_children = len(selected)
    mates = population.sample(len(selected), replace=True)
    save_index = mates.index[0]
    mates.index = range(len(mates))
    selected.index = range(len(selected))
    tinder = pd.merge(selected, mates, left_index=True, right_index=True, suffixes=('_s', '_m'))
    sel_or_mates = np.random.choice([0, n_cols], size=(n_children, n_cols), p=(.3, .7))
    indices = sel_or_mates + np.array([range(n_cols)] * n_children)
    child_data = tinder.values[:, indices][range(n_children), range(n_children)]
    children = pd.DataFrame(data=child_data,
                            columns=selected.columns,
                            index=[save_index] * n_children)
    return children


def crossover_selection(chromozomes, size):
    save_index = chromozomes.index[0]
    chromozomes.index = range(len(chromozomes))
    min_v = (1 / len(chromozomes))
    max_v = (3 + min_v)
    chromozomes['proba'] = min_v + (max_v - min_v) * chromozomes['rank'] / (len(chromozomes) - 1)
    chromozomes['proba'] /= sum(chromozomes['proba'])
    selected_indices = np.random.choice(chromozomes.index, size=size, p=chromozomes['proba'])
    del chromozomes['proba']
    selected = chromozomes.loc[selected_indices]
    selected.index = [save_index] * len(selected)
    chromozomes.index = [save_index] * len(chromozomes)
    return selected


def norm_diff(X):
    diff_mat = abs(np.diff(X, axis=0, prepend=0)) + abs(np.diff(X, axis=1, prepend=0))
    return np.linalg.norm(diff_mat)


def fitness(mnist_base, chromozomes, sample, target_output):
    pred_p = mnist_base.model_proba(chromozomes)
    pred = np.argmax(pred_p, axis=1)
    sample_output = mnist_base.model(sample)[0]
    matching = np.max(pred_p[:, np.arange(pred_p.shape[1]) == target_output], axis=1) - .3 * pred_p[:, sample_output]
    # matching = (mnist_base.model(chromozomes)!=mnist_base.model(sample))
    if mnist_base.representation_type == 'blur':
        closeness = - chromozomes.mean(axis=1) * .1
    else:
        closeness = chromozomes.isnull().mean(axis=1)
    chromozomes = chromozomes.fillna('nan')
    similarity = np.array([np.mean(chromozomes.values == chromozomes.values[idx]) for idx in range(len(chromozomes))])
    #     similarity = np.mean([pred == i for i in pred], axis=1)
    chromozomes = chromozomes.replace('nan', -0.000123456789)
    grad_norm = .1 * chromozomes.apply(lambda x: norm_diff(x.values.reshape(NPIX, NPIX)), axis=1)
    chromozomes = chromozomes.replace(-0.000123456789, np.nan)
    if sum(pred == target_output) <= 30:
        return matching, (
        round(matching.mean(), 2), round(closeness.mean(), 2), round(similarity.mean(), 2), round(grad_norm.mean(), 2))
    else:
        return matching - similarity - grad_norm, (
        round(matching.mean(), 2), round(closeness.mean(), 2), round(similarity.mean(), 2), round(grad_norm.mean(), 2))


def info_pop(mnist_base, chromozomes, fitness_vector, n_cols):
    print(mnist_base.model(chromozomes).reshape(n_cols, int(len(chromozomes) / n_cols)))
    print(fitness_vector)
    mnist_base.display_rows(chromozomes, n_cols)


def find_counterfactual_evol(mnist_base, sample, parent_size, child_size, new_size, binary, fit_similarity,
                             target_output):
    chromozomes = rdn_samples(mnist_base, sample, parent_size, binary)
    fitness_vector = fitness(mnist_base, chromozomes, sample, target_output)
    chromozomes['fitness'] = fitness_vector[0]
    chromozomes.sort_values('fitness', ascending=True, inplace=True)
    chromozomes['rank'] = range(len(chromozomes))
    fitness_graph = {'mean': [], 'std': [], 'max': [], 'match': [], 'closeness': [], 'similarity': [], 'grad': []}
    for _ in range(1000):
        plt.rcParams['figure.figsize'] = [15, 3]
        mate_selection = crossover_selection(chromozomes, child_size)
        #         print("Selected")
        #         info_pop(mnist_base, mate_selection, fitness(mnist_base, mate_selection, sample, fit_similarity), n_cols)
        children = crossover(mate_selection[mnist_base.feature_names], chromozomes[mnist_base.feature_names])
        #         print("Children")
        #         info_pop(mnist_base, children, fitness(mnist_base, children, sample, fit_similarity), n_cols)
        chromozomes = update_population(mnist_base, sample, children, chromozomes, new_size, binary)
        #         print("Updated pop")
        #         info_pop(mnist_base, chromozomes, fitness(mnist_base, chromozomes, sample, fit_similarity), n_cols)
        chromozomes = mutate(chromozomes, mnist_base, sample, binary)
        #         print("Mutants")
        #         info_pop(mnist_base, chromozomes, fitness(mnist_base, chromozomes, sample, fit_similarity), n_cols)
        fitness_vector = fitness(mnist_base, chromozomes, sample, target_output)
        chromozomes['fitness'] = fitness_vector[0]
        chromozomes.sort_values('fitness', ascending=True, inplace=True)
        chromozomes['rank'] = range(len(chromozomes))
        fitness_graph['mean'].append(fitness_vector[0].mean())
        fitness_graph['std'].append(fitness_vector[0].std())
        fitness_graph['max'].append(fitness_vector[0].max())
        fitness_graph['match'].append(fitness_vector[1][0])
        fitness_graph['closeness'].append(fitness_vector[1][1])
        fitness_graph['similarity'].append(fitness_vector[1][2])
        fitness_graph['grad'].append(fitness_vector[1][3])
        if _ % 50 == 0:
            print("GA for target", target_output, " iteration ", _)
    #         if _ % 50 == 0:
    #             n_cols = int(len(chromozomes.tail(50)) / 5)
    #             plt.rcParams['figure.figsize'] = [15, 2 * n_cols]
    #             print('mean', round(fitness_vector[0].mean(), 2),
    #                   'std', round(fitness_vector[0].std(), 2),
    #                   "match, close, sim", fitness_vector[1])
    #             print(np.array(mnist_base.model(chromozomes.tail(50))).reshape(n_cols, int(len(chromozomes.tail(50)) / n_cols)))
    #             mnist_base.display_rows(chromozomes.tail(50), nrows=n_cols)
    #         if _ % 100 == 0 and _ > 0:
    #             plt.rcParams['figure.figsize'] = [15, 3]
    #             plt.plot(range(_ + 1), fitness_graph['mean'], label='mean')
    #             plt.plot(range(_ + 1), fitness_graph['std'], label='std')
    #             plt.plot(range(_ + 1), fitness_graph['max'], label='max')
    #             plt.plot(range(_ + 1), fitness_graph['match'], label='match')
    #             plt.plot(range(_ + 1), fitness_graph['closeness'], label='closeness')
    #             plt.plot(range(_ + 1), fitness_graph['similarity'], label='similarity')
    #             plt.plot(range(_ + 1), fitness_graph['grad'], label='gradient norm')
    #             plt.legend()
    #             plt.show()
    #             chromozomes['fitness'].hist(bins=20)
    #             plt.show()
    #             if abs(np.mean(fitness_graph['mean'][-20:]) - np.mean(fitness_graph['mean'][-100:])) < np.mean(
    #                     fitness_graph['mean'][-100:]) * .01:
    #                 break
    return chromozomes
