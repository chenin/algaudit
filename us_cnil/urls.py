"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from cnil.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', WelcomeView.as_view(), name='welcome_page'),
    path('choose/tool', ChoiceToolView.as_view(), name='choose_tool'),
    path('presentation/', PresentationView.as_view(), name='presentation'),
    path('consent/', ConsentView.as_view(), name='consent'),
    path('presentation/features/', FeaturesPresentationView.as_view(), name='features_presentation'),
    path('example/decision', DecisionExampleView.as_view(), name="decision_example"),
    path('example/decision/final', DecisionExampleFinalView.as_view(), name="decision_example_final"),
    path('presentation/ibex', PresentationIbexView.as_view(), name="ibex_presentation"),
    path('example/ibex', IbexView.as_view(template_name="ibex_example.html"), name="ibex_example"),
    path('task/ibex', IbexView.as_view(template_name="ibex_task.html"), name="ibex_task"),
    path('final/ibex', FinalIbexView.as_view(), name="ibex_final"),
    path('presentation/algocate', PresAlgocateView.as_view(), name="algocate_presentation"),
    path('example/algocate', AlgocateView.as_view(template_name="algocate_example.html") ,name="algocate_example"),
    path('task/algocate', AlgocateView.as_view(template_name="algocate_task.html"), name="algocate_task"),
    path('final/algocate', FinalAlgocateView.as_view(), name="algocate_final"),
    path('thanks', Thanks.as_view(), name='thanks'),
]
