L'organisation conseillée pour le dossier du code est la suivante : 

```
.
├── env
│   ├── bin
│   ├── lib
│   ├── pyvenv.cfg
│   └── share
└── src
    ├── cnil
    ├── data
    ├── db.sqlite3
    ├── delivering
    ├── explanation
    ├── explore.ipynb
    ├── generation
    ├── justify
    ├── manage.py
    ├── README.md
    ├── requirements.txt
    ├── sampling
    ├── settings
    ├── shared
    ├── static
    ├── templates
    └── us_cnil
```


En pratique :

```
 mkdir algaudit
 cd algaudit
 git clone https://gitlab.inria.fr/chenin/algaudit.git
 mv algaudit src
```

Ensuite on crée l'environnement virutel :

```
 virtualenv -p python3 env
 source env/bin/activate
 cd src
 pip install -r requirements.txt
```

Vous devriez désormais pouvoir executer l'application en local avec la commande suivante : 

```python manage.py runserver```

ou bien: 

```python3 manage.py runserver``` 

(en fonction de la version de python par défaut)