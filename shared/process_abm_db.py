import pandas as pd
import numpy as np
import datetime as dt
import os

from mysite.settings import *

MALADIE_CODE = {
    "Inconnue ou indéterminée": "0", "Cmno idiopathique": "151", "Cmno familiale": "152",
    "Myocardite": "153", "Cmno du post partum": "154", "Cmno toxique": "155", "Cardiopathie autre origine": "156",
    "Cardiopathie restrictive": "157", "Cardiopathie hypertrophique": "158", "Maladie coronarienne": "159",
    "Tumeur cardiaque": "160", "Chirurgie thoracique antérieure": "161", "Maladie valvulaire": "162",
    "Maladie congénitale": "163", "Cmno due à l'alcool": "164", "Bronchiolite oblitérante": "803",
    "Dilatation des bronches": "809", "Hypertension pulmonaire primitive": "810", "Eisenmenger": "811",
    "Eisenmenger a.s.d.": "812", "Eisenmenger v.s.d.": "813", "Eisenmenger p.d.a.": "814",
    "Maladie congénitale non Eisenmenger": "815", "Mucoviscidose": "818", "Déficit en alpha 1 antitrypsine": "819",
    "Fibrose pulmonaire": "820", "Hypertension pulmonaire secondaire": "821", "Inhalation": "822",
    "Emphysème": "823", "Maladie d'origine toxique": "825", "BPCO": "826", "Hypertension artérielle primitive": "827",
    "Histiocytose x": "899", "Retransplantation cause échec non précisée": "900", "Retransplantation rejet hyper-aigu": "901",
    "Retransplantation rejet chronique": "902", "Retransplantation rejet aigu": "910",
    "Retransplantation cause rectrictive": "957",
    "Autre": "999", }


COL_NAME_DICT_R = {
    'NATT_ANON': 'NATT_ANON', 'AVION': 'ttlgp', 'DNAISSR_ANON': 'age_r',
    'SEXER': 'sex_r', 'ABOR': 'abo_r', 'TAILLER': 'taille_r',
    'POIDINS': 'poids_r', 'URGENCE': 'urgence', 'DURGENCE': 'daurg',
    'LMALADI': 'mal_set', 'LMALADIE2': 'mal_set', 'LMALADIE3': 'mal_set',
    'XPC': 'xpc', 'DATB_ANON': 'da', 'DRG2': 'drg', 'CEC2': 'cec',
    'DCEC2_ANON': 'delay_cec', 'SIAV2': 'siav', 'CAT2': 'cat', 'BNP2': 'bnp',
    'DBNP2_ANON': 'dt_bnp', 'PROBNP2': 'probnp', 'DPROBNP2_ANON': 'dt_probnp',
    'DIA2': 'dialyse', 'CREAT2': 'creat', 'DCREAT2_ANON': 'dt_creat',
    'BILI2': 'bili', 'DBILI2_ANON': 'dt_bili', 'BNP_AVI': 'bnp_avi',
    'PBN_AVI': 'probnp_avi', 'DIA_AVI': 'dialyse_avi', 'CRE_AVI': 'creat_avi',
    'BILI_AVI': 'bili_avi',
}

COL_NAME_DICT_D = {
    'SEX': 'sex_d', 'AGE': 'age_d', 'ABO': 'abo_d',
    'TAI': 'taille_d', 'POI': 'poids_d', 'DATPREL_ANON': 'dt_today'
}


def load_data():
    cand = pd.read_excel(os.path.join(BASE_DIR, "data/dataset/COHORTE_CAND_ANONYM.xlsx"))
    urg_anon = pd.read_excel(os.path.join(BASE_DIR, "data/dataset/CAND_DURGENCE_ANONYM.xlsx"))
    bilan = pd.read_excel(os.path.join(BASE_DIR, "data/dataset/BIL_CAND_ANONYM.xlsx"))
    cit = pd.read_excel(os.path.join(BASE_DIR, "data/dataset/CIT_CAND_ANONYM.xlsx"))
    donneur = pd.read_excel(os.path.join(BASE_DIR, "data/dataset/COHORTE_DON_ANONYM.xlsx"))
    dist = pd.read_excel(os.path.join(BASE_DIR, "data/dataset/PDIST.xlsx"))
    return cand, urg_anon, bilan, cit, donneur, dist


def merge_cand_bilan_cit(cand, urg_anon, bilan, cit):
    # merge cand with urg_anon to use the good value of DATE URGENCE
    cand_with_durg = pd.merge(cand, urg_anon, on="NATT_ANON", how='left')
    cand_with_durg.drop('DURGENCE', inplace=True, axis=1)
    cand_with_durg.rename(columns={"DURGENCE_ANON": "DURGENCE"}, inplace=True)

    # keep only candidates having at least one bilan (57 candidates have no bilan)
    cb_df = pd.merge(cand_with_durg, bilan, on="NATT_ANON", how='left')  # create a row for each pair cand / bilan
    cb_df['CB_ID'] = cb_df.index  # unique identifier

    # add end date to bilan
    cb_df['NEXT_DINS'] = cb_df.groupby(['NATT_ANON'])['DATB_ANON'].shift(-1)  # end period = next bilan
    # end period = DCD or GRF
    array_bool_dcd_grf = (cb_df['ETATLA'].apply(lambda x: x in ['GRF', 'DCD']))
    cb_df.loc[(cb_df['NEXT_DINS'].isnull()) & array_bool_dcd_grf, 'NEXT_DINS'] = cb_df['DETATLA_ANON']
    # end period = last bilan + 3 months
    cb_df.loc[cb_df['NEXT_DINS'].isnull(), 'NEXT_DINS'] = cb_df['DATB_ANON'] + dt.timedelta(days=90)

    # convert maladie initiale to code
    cb_df.loc[:, 'LMALADI'] = cb_df['LMALADI'].replace(MALADIE_CODE)
    cb_df.loc[:, 'LMALADIE2'] = cb_df['LMALADIE2'].replace(MALADIE_CODE)
    cb_df.loc[:, 'LMALADIE3'] = cb_df['LMALADIE3'].replace(MALADIE_CODE)

    # remove CIT periods
    # date axis (CIT and bilans) are projected on this axis
    n_days = (cb_df['NEXT_DINS'].max() - cb_df['DATB_ANON'].min()).days
    date_list = [cb_df['DATB_ANON'].min() + dt.timedelta(days=x) for x in range(-2, n_days + 2)]

    cbc_df = pd.DataFrame(columns=cb_df.columns)

    # for each patient project begin and end date and all CIT on a date axis
    for cb_row in cb_df.iterrows():
        # create a date axis (from the first until the last day of the db)
        patient_cit = cit[cit['NATT_ANON'] == cb_row[1]['NATT_ANON']]
        # list of bool: True if active inscription, False otherwise
        cb_day_list = [cb_row[1]["DATB_ANON"] <= x <= cb_row[1]["NEXT_DINS"] for x in date_list]
        cit_day_list = [[False] * len(date_list)]
        for row in patient_cit.iterrows():
            # list of bool: True if active CIT False otherwise
            cit_day_list.append([row[1]["DDEB_ANON"] <= x <= row[1]["DFIN_ANON"] for x in date_list])
        # true is any CIT is active
        cit_date_axis = np.any(cit_day_list, axis=0)
        # inscription must be active, but no cit
        cd_no_cit_day_list = np.all([~cit_date_axis, cb_day_list], axis=0)
        # "derivative" of the previous list to get begin and end dates
        cb_no_cit_bg_end_dates = np.array(date_list)[np.roll(cd_no_cit_day_list, 1) ^ cd_no_cit_day_list]
        for idx in range(len(cb_no_cit_bg_end_dates)):
            if idx % 2 == 0:
                values = cb_row[1].to_dict()
                values["DATB_ANON"] = cb_no_cit_bg_end_dates[idx]
                values["NEXT_DINS"] = cb_no_cit_bg_end_dates[idx + 1] - dt.timedelta(days=1)
                cbc_df = cbc_df.append(values, ignore_index=True)
    long_period = (cbc_df['NEXT_DINS'] - cbc_df['DATB_ANON']) > dt.timedelta(days=90)
    cbc_df.loc[long_period, 'NEXT_DINS'] = cb_df['DATB_ANON'] + dt.timedelta(days=90)
    return cbc_df


def merge_cbc_donneur(cbc_df, donneur):

    # full outer join (every bilan matcher with every donneur)
    cbc_df.loc[:, 'key'] = 1
    donneur.loc[:, 'key'] = 1
    donneur = donneur.rename(columns=COL_NAME_DICT_D)
    donneur.loc[:, 'sex_d'] = donneur['sex_d'].replace({'M': 'H'})

    cbcd_df = pd.merge(cbc_df, donneur, on='key', how='outer')

    # only keep donneur that appeared after bilan and before next bilan 
    cbcd_df = cbcd_df.loc[
        (cbcd_df['DATB_ANON'] <= cbcd_df['dt_today']) & (cbcd_df['dt_today'] <= cbcd_df['NEXT_DINS'])]
    del cbcd_df['key'], cbc_df['key'], donneur['key']
    return cbcd_df


def merge_cbcd_dist(cbcd_df, dist):
    # add transportation time
    dist.columns = ['EQUIPGRF', 'SITPREL', 'AVION']
    cbcdt_df = pd.merge(dist, cbcd_df, on=['EQUIPGRF', 'SITPREL'], how='inner')
    return cbcdt_df


def data_formating(cbcdt_df):
    # change columns naming
    cbcdt_df = cbcdt_df.rename(columns=COL_NAME_DICT_R)

    if cbcdt_df['cec'].dtype != bool:
        cbcdt_df = cbcdt_df.replace({'cec': {np.nan: False, 'N': False, 'O': True}})
    cbcdt_df.loc[:, 'DCEC'] = cbcdt_df['delay_cec'].copy()
    cbcdt_df.loc[:, 'DURG'] = cbcdt_df['daurg'].copy()
    cbcdt_df.loc[:, 'DATB_ANON'] = cbcdt_df['da'].copy()
    cbcdt_df.loc[:, 'DATPREL_ANON'] = cbcdt_df['dt_today'].copy()
    cbcdt_df.loc[:, 'age_r'] = (cbcdt_df['dt_today'] - cbcdt_df['age_r']).apply(lambda x: int(x.days / 365))
    cbcdt_df.loc[:, 'delay_cec'] = (cbcdt_df['dt_today'] - cbcdt_df['delay_cec']).apply(lambda x: x.days)
    cbcdt_df.loc[:, 'xpc'] = cbcdt_df['xpc'] * 30
    cbcdt_df.loc[:, 'daurg'] = (cbcdt_df['dt_today'] - cbcdt_df['daurg']).apply(lambda x: x.days)
    cbcdt_df.loc[:, 'da'] = (cbcdt_df['dt_today'] - cbcdt_df['da']).apply(lambda x: x.days)
    cbcdt_df.loc[:, 'dialyse'] = cbcdt_df['dialyse'].replace({'N': None})
    cbcdt_df.loc[cbcdt_df['daurg'] < 0, 'urgence'] = np.nan
    cbcdt_df.loc[cbcdt_df['urgence'].isnull(), 'xpc'] = np.nan
    cbcdt_df.loc[cbcdt_df['daurg'] < 0, 'daurg'] = np.nan
    cbcdt_df.loc[:, 'urgence'] = cbcdt_df['urgence'].replace({np.nan: None})
    cbcdt_df.loc[:, 'sex_r'] = cbcdt_df['sex_r'].replace({'M': 'H'})
    cbcdt_df.loc[:, 'mal_set_'] = cbcdt_df.apply(lambda x: set(x['mal_set'].values), axis=1)
    del cbcdt_df['mal_set']
    cbcdt_df = cbcdt_df.rename(columns={'mal_set_': 'mal_set'})
    cbcdt_df.loc[:, 'mal_set'] = cbcdt_df['mal_set'].apply(lambda x_set: {x for x in x_set if not np.isnan(x)})
    return cbcdt_df


def generate_data_set(share=1.):
    # load and merge data from abm
    cand, urg_anon, bilan, cit, donneur, dist = load_data()
    cand = cand.sample(frac=share)
    cbc_df = merge_cand_bilan_cit(cand, urg_anon, bilan, cit)
    cbcd_df = merge_cbc_donneur(cbc_df, donneur)
    cbcdt_df = merge_cbcd_dist(cbcd_df, dist)

    return data_formating(cbcdt_df)

#
# def load_saved_data():
#     data = pd.read_csv("/home/clement/Encfs/abm_data/sc_explanations/data/dataset/cbcdt.csv")
#     for dt_col in ["DINSCMED_ANON", "DETATLA_ANON", "dt_bnp", "dt_probnp", "dt_creat", "dt_bili", "NEXT_DINS", "dt_today", "DURG", "DATB_ANON", "DATPREL_ANON"]:
#         data.loc[:, dt_col] = pd.to_datetime(data[dt_col])
#     data.loc[:, 'mal_set'] = data['mal_set'].apply(eval)
#     return data
#
#
# def generate_csv_file():
#     data = generate_data_set()
#     data.to_csv("/home/clement/Encfs/abm_data/sc_explanations/data/dataset/cbcdt_.csv",
#                 index=False)