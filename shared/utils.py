import pandas as pd
import numpy as np


def tuplize_columns(df, col_tuple):
    """
    Replace columns of col_tuple into one column containing a tuple of
    values.
    :param df: dataframe containing at least the col_tuples columns
    :param col_tuple: tuple of columns to be tuplized (col1, col2)
    :return: tuplized dataframe with column col1_col2
    """
    df_copy = df.copy()
    for col in col_tuple:
        try:
            assert col in df_copy.columns
        except AssertionError as e:
            e.args += ('Column ', col, " should be a column of df.", )
            raise
    v_name = '_'.join(col_tuple)
    coup_func = lambda row: tuple(row[v] for v in col_tuple)
    df_copy[v_name] = df_copy.apply(coup_func, axis=1)
    for col in col_tuple:
        del df_copy[col]
    return df_copy


def flatten_array(array):
    return [item for sublist in array for item in sublist]


def apply_mask_image(img, mask, mask_idx):
    repeat_factor = int(img.shape[0] / mask_idx.shape[0]) + (img.shape[0] % mask_idx.shape[0]) * 1
    imsize_mask = np.repeat(mask_idx, repeat_factor, axis=1)
    imsize_mask = np.repeat(imsize_mask, repeat_factor, axis=0)[:img.shape[0], :img.shape[0]]
    img[imsize_mask] = mask[imsize_mask]


def shuffle_along_axis(a, axis):
    idx = np.random.rand(*a.shape).argsort(axis=axis)
    return np.take_along_axis(a,idx,axis=axis)


def simulate_uniform_draw_counts(n_draws, support_size):
    p = {}
    for k in range(1, support_size):
        p[k] = np.random.binomial(n_draws - sum(p.values()), 1. / (support_size - k))
    return p


def simulate_normal_draw_counts(n_draws, support_size, sigma):
    simu = np.unique(abs(np.random.normal(0, sigma, size=n_draws).astype(int)), return_counts=True)
    counts = dict(zip(simu[0] + 1, simu[1]))
    for key in list(counts.keys()).copy():
        if key > support_size:
            counts[1] += counts[key]
            del counts[key]
    return counts


def replace_col_values(original_df, new_val_df, col_array):
    """
    Exchange values from original_df with values of new_val_df.
    This function can be used in different ways.
    If len(new_val_df) is one,
    then the same values are use for every row of original_df.
    Else (original_df and new_val_df should be of the same size),
    each value of original_df is replaced by a randomly picked
    value from new_val_df.
    If col_array contains only one element,
    only one column is replaced everywhere in original_df.
    Else len(col_array) should be the size of original_df
    for each row of orignal_df a different combination of
    columns is used.
    :param original_df: dataframe containing values that will be replaced
    :param new_val_df: dataframe containing values that replaces the original ones
    :param col_array: list of set of column(s) to be exchanged
    :return: dataframe with replaced values
    """
    # creating a joined dataframe that gathers every values to be replaced
    index_save = original_df.index
    left = original_df.copy()
    right = new_val_df.copy()
    if len(right)==1:
        left['key'] = 1
        right['key'] = 1
        # cartesian product
        joined = pd.merge(
            left, right, on='key', suffixes=('', '_new')
            ).drop('key', axis=1)
        left.drop('key', axis=1, inplace=True)
        right.drop('key', axis=1, inplace=True)
    elif len(left)==len(right):
        # one to one matching with random permutation
        right.index = np.random.permutation(right.index)
        joined = left.join(right, rsuffix='_new')
    else:
        raise ValueError("new_values_df should be of size 1 OR of size len(original_df)")
    if len(col_array)==1:
        # when one set of columns is specified, change this set for every row
        cols_tuple = tuple(col_array[0])
        new_cols_tuple = [c + '_new' for c in cols_tuple]
        joined.loc[:, cols_tuple] = joined.loc[:, new_cols_tuple].values
    elif len(col_array)==len(joined):
        # one set of columns is used for each row
        for cols in np.unique(np.array(col_array)):
            sel_bool_array = (np.array(col_array) == cols)
            cols_tuple = tuple(cols)
            new_cols_tuple = [str(c) + '_new' for c in cols_tuple]
            joined.loc[sel_bool_array, cols_tuple] = joined.loc[sel_bool_array, new_cols_tuple].values
    for col in left.columns:
        joined.drop(str(col) + '_new', axis=1, inplace=True)
    return joined.set_index(index_save)
